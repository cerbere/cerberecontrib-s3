=================
cerberecontrib-s3
=================

Cerbere reader classes for various EUMETSAT Sentinel-3 products, including:

* S3SLWCT: SLSTR WCT SAFE files
* S3SLWST : SLSTR WST SAFE files (early convention and day 2 collection format)
* S3SLIST: SLSTR IST SAFE files

For SLSTR WST datasets (sea surface temperature), you should provide the 
path to the SAFE folder for early convention files (containing a single 
NetCDF file with bot nadir and dual view fields) but for newer convention 
(day 2 collection) you should provide the full path to either the nadir or 
dual view netcdf file (now provided in two different files).


.. code-block:: python

  # example : opening a Sentinel-3A SLSTR old multi-view SAFE
  import cerbere

  dst = cerbere.open_dataset(
      'S3A_SL_2_WST____20210915T010108_20210915T010408_20210915T031329_0179_076_202_2700_MAR_O_NR_003.SEN3',
      reader='S3SLWST'
  )


.. code-block:: python

  # example : opening a newer Sentinel-3A SLSTR dual-view file
  import cerbere

  dst = cerbere.open_dataset(
      'S3A_SL_2_WST____20250120T091236_20250120T091536_20250120T114407_0180_121_321______EUM_P_NR_004.SEN3/20250120091236-EUM-L2P_GHRSST-SSTskin-SLSTRA_NRT_DUAL_20250120114407-v02.1-fv01.0.nc',
      reader='S3SLWST'
  )
