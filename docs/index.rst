.. cerberecontrib-s3 documentation master file, created by
   sphinx-quickstart on Mon Oct 24 10:21:42 2016.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to cerberecontrib-s3's documentation!
=============================================

This project provides additional mappers for cerbere library for Sentinel-3
products in SAFE format.

Contents:

.. toctree::
   :maxdepth: 2

   install
   slstr_recipe


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

