=============================
Reading Sentinel-3 SLSTR data
=============================

Like with any **cerbere** mapper, you have to instantiate a mapper of the correct
class for this product type. Note the cerbere does here a certain number of things
for you:

* accessing a SAFE folder as a unique virtual file. You don't have to open any 
  specific netcdf file within the SAFE. The SAFE is read as if all fields and attributes
  were contained in a single file. There are some restrictions to that (see
  next section).
* merging nadir or oblique views onto the same product grids, with the same
  dimensions (area not covered by oblique view are filled in with fill values).
* simplifying some variables, such as time which is provided as a unique field.
  rather than a combination of multiple fields to combine together
* interpolating subsampled fieds on the data grid (such as meteo or geometry fields).


Sub-products
============

There is not a single mapper for all SLSTR product types and, worst, there is not 
even a single mapper for the same SAFE product.

This is due to the fact that the same SAFE product mixes different views and
grids, each with different dimensions.

.. important::

  From **cerbere** perspective, each grid is considered as a different "sub-product",
  with its own mapper class. However it merges oblique and nadir views as if
  they were contained in the same file, so you don't need to instanciate a
  mapper for each view.

Note that all mapper classes inherit from the same parent class, `SAFESLFile`.

The following table defines which mapper you need to use depending of the channels or views
of interest for you. Note the unique relation between the file suffixes in SAF netcdf files
(_in, _io, etc...) and a **cerbere** mapper:

+------------------------------------------------+------------------+-----------+
| Dataset                                        | Mapper class     | Datamodel |
+================================================+==================+===========+
| S3A_SL_2_WCT Nadir (in)                        | SAFESLIRFile     | Swath     |
| S3A_SL_1_RBT 1km Nadir (in)                    |                  |           |
| S3A_SL_2_WCT Oblique (io)                      |                  |           |
| S3A_SL_1_RBT 1km Oblique (io)                  |                  |           |
+------------------------------------------------+------------------+-----------+
| S3A_SL_1_RBT 500m & SWIR A Stripe Nadir (an)   | SAFESL500AFile   | Swath     |
| S3A_SL_1_RBT 500m & SWIR A Stripe Oblique (ao) |                  |           |
+------------------------------------------------+------------------+-----------+
| S3A_SL_1_RBT 500m & SWIR B Stripe Nadir (bn)   | SAFESL500BFile   | Swath     |
| S3A_SL_1_RBT 500m & SWIR B Stripe Oblique (bo) |                  |           |
+------------------------------------------------+------------------+-----------+
| S3A_SL_1_RBT TDI Nadir (cn)                    | SAFESL500TDIFile | Swath     |
| S3A_SL_1_RBT TDI Oblique (co)                  |                  |           |
+------------------------------------------------+------------------+-----------+

Discovery of the file content
=============================

Opening a file
--------------

Let's for instance work with a SAFE L1 RBT product::

   S3A_SL_1_RBT____20161015T155616_20161015T155916_20161015T175945_0179_010_011_4139_MAR_O_NR_002.SEN3

We are interested in the 1km infra-red channels, so we will use ``SAFESLIRFile`` mapper from above table:

.. code-block:: python

   >>> from cerbere.mapper.safeslfile import SAFESLIRFile
   >>> fname = 'S3A_SL_1_RBT____20161015T155616_20161015T155916_20161015T175945_0179_010_011_4139_MAR_O_NR_002.SEN3'
   
   >>> # this creates the mapper object (equivalent to opening a file)
   >>> fd = SAFESLIRFile(url=fname)
   >>> fd.open()


Similarly to the NetCDF API, **cerbere** provides methods to inspect the
content of a file as if it was self described (which is not the case of all
formats).

Reading fields
--------------

Reading and printing the list of fields:

.. code-block:: python

   >>> for _ in f.get_fieldnames(): print _
   solar_zenith_to
   solar_zenith_tn
   sat_zenith_tn
   sat_zenith_to
   S9_FEE_offset_io
   S9_FEE_offset_in
   S9_cal_offset_io
   S9_cal_offset_in
   S8_BBEU_Bridge_Ref_io
   S8_BBEU_Bridge_Ref_in
   S7_bandwidth_io
   S7_bandwidth_in
   ...

.. note::
  Note that only the geophysical fields are returned. Geolocation (spatial and
  temporal) fields are filtered out.

Getting a specific field and printing its properties:

.. code-block:: python

   >>> field = fd.read_field('solar_zenith_tn')
   >>> print field
   field : solar_zenith_tn
      dimensions :
           # row : 1200
           # cell : 1500
      attributes :
           # long_name : Solar zenith angle
           # standard_name : solar_zenith_angle
           # source_file : geometry_tn.nc
           # source_variable : solar_zenith_tn
      conventions :
           # Units : degrees
           # Fillvalue : None


Note in the result above the dimensions of the field. These are not the original
dimensions (the number of rows would be 130). This is because all auxiliary fields
such as geometry or meteo field are nicely resampled on the 1km product grid. If you
use another subproduct mapper - like ``SAFESL500TDIFile`` for TDI grid, they will be
resampled on the TDI grid instead.

Reading dimensions
------------------

Getting the names of the file dimensions, returned as a list:

.. code-block:: python

   >>> dims = fd.get_dimensions()
   >>> print dims
   ['cell', u'orphan_pixels_n', u'orphan_pixels_o', 'row']

Getting the dimension names of a given field, as a tuple matching
their order in the data array:

.. code-block:: python

   >>> dims = fd.get_dimensions(fieldname='solar_zenith_tn')
   >>> print dims
   ('row', 'cell')
  
   >>> # or inquiring the field object directly
   >>> field = fd.read_field('solar_zenith_tn')
   >>> dims = field.get_dimnames()
   ('row', 'cell')

You get a dictionary of both the dimensions names and sizes with:

.. code-block:: python

   >>> # some field's dimensions
   >>> dims = fd.get_full_dimensions(fieldname='solar_zenith_tn')
   >>> print dims
   OrderedDict([('row', 1200), ('cell', 1500)])
   
   >>> # or, with the field object :
   >>> field = fd.read_field('SST')
   >>> size = field.get_dimsize('row')
   OrderedDict([('row', 1200), ('cell', 1500)])

The size of a specific dimension can also be easily retrieved:

.. code-block:: python

   >>> print fd.get_dimsize('cell')
   1500

Reading attributes
------------------

Getting the global attributes. The following returns a "basic" list of attributes
(the global attributes existing in the netcdf files within each SAFE product):

.. code-block:: python

   >>> attributes = fd.read_global_attributes()
   >>> for attr in attributes:
   >>>     print attr, fd.read_global_attribute(attr)
   comment  
   contact ops@eumetsat.int
   creation_time 20161015T175945Z
   institution MAR
   netCDF_version 4.2 of Jul  5 2012 17:07:43 $
   product_name S3A_SL_1_RBT____20161015T155616_20161015T155916_20161015T175945_0179_010_011_4139_MAR_O_NR_002.SEN3
   references S3IPF PDS 005 - i1r9 - Product Data Format Specification - SLSTR, S3IPF PDS 002 - i1r5 - Product Data Format Specification - Product Structures, S3IPF DPM 003 - i1r3 - Detailed Processing Model - SLSTR Level 1
   resolution [ 1000 1000 ]
   source IPF-SL-1 06.03
   start_offset 1907
   start_time 2016-10-15T15:56:17.028927Z
   stop_time 2016-10-15T15:59:16.720066Z
   title S3 SLSTR L1 Radiance and Brightness Temperatures Product. (Measurements + Annotations)
   track_offset 998

An extended list of attributes can be retrieved from the SAFE product's **manifest** file.
You can get these additional attributes using the ``extended`` keyword.

.. code-block:: python

   >>> attributes = fd.read_global_attributes(extended=True)
   >>> for attr in attributes:
   >>>     print attr, fd.read_global_attribute(attr)

Note that when looking for the value of a specific attribute, you don't need to
mention if this is a basic attribute or an attribute from the manifest. The
mapper will automatically search for a matching value in both:

.. code-block:: python

   >>> print "Orbit", fd.read_global_attribute("absolute_orbit_number")
   Orbit  3449

Getting some specific (normalised) attributes:

.. code-block:: python

   >>> # start time
   >>> print fd.get_start_time()
   2016-10-15 15:56:34.028927
   
   >>> # end time
   >>> print fd.get_end_time()
   2016-10-15 15:59:33.720066

Reading values
--------------

Getting the values of any field:

.. code-block:: python

   >>> data = fd.read_values('solar_zenith_tn')
   >>> print data
   [[-- -- -- ..., -- -- --]
    [-- -- -- ..., -- -- --]
    [-- -- -- ..., -- -- --]
    ..., 
    [-- 76.43095492328901 76.4309673361432 ..., -- -- --]
    [-- 76.43096256740472 76.43097498025882 ..., -- -- --]
    [-- -- -- ..., -- -- --]]
 
Getting a subset using a tuple of slices on spatial dimensions. More flexible slicing
can be used in combination with **cerbere** ``datamodel`` classes, see further:

.. code-block:: python

   >>> data = fd.read_values('solar_zenith_tn', (slice(10,13), slice(30, 33)))
   [[67.54784545169854 67.55156758812822 67.5515655536872]
    [67.54786421498878 67.55157497023708 67.55157293579555]
    [67.56291278201861 67.56656320238262 67.56656116686756]]


if you don't use a ``datamodel``, the geolocation information has to be queried like any field:

.. code-block:: python

   >>> lat = fd.read_values('lat')
   >>> lon = fd.read_values('lon')

   >>> times = fd.read_values('time')

   >>> # or, using fields
   >>> field = fd.read_field('time')
   >>> times = field.get_values()

   >>> # convert to datetime object
   >>> from netCDF4 import num2date
   >>> field = fd.read_field('time')
   >>> times = field.get_values()
   >>> print num2date(times[:], field.units)

.. note::
  `lat`, `lon`, `z` and `time` are standardized geolocation field names and will
  work with any mapper (whatever internal naming was used in the native file format)



Using a data model
==================

The content of the file can be mapped into a data model which is convenient for
operations using these datamodel.

In the case of the L1 or L2 products, we will use the ``Swath`` model
as listed in the above table.:

.. code-block:: python

   >>> from cerbere.datamodel.swath import Swath
   >>> swath = Swath()


Load the content from a file into the model, thanks to the mapper already
created, using the ``load`` method:

.. code-block:: python

   >>> from cerbere.mapper.safeslfile import SAFESLFile
   >>> fname = 'S3A_SL_1_RBT____20161015T155616_20161015T155916_20161015T175945_0179_010_011_4139_MAR_O_NR_002.SEN3'
   >>> # this creates the mapper object (equivalent to opening a file)
   >>> fd = SAFESLFile(url=fname)
   >>> swath.load(fd)

To read the lat, lon, z and times, specific methods are provided:

.. code-block:: python

   >>> lats = swath.get_lat()
   >>> lons = swath.get_lon()
   >>> times = swath.get_times()

In above example, times are returned as numbers than can be converted to
datetime objects geting first the units:

.. code-block:: python

   >>> units = swath.get_time_units()
   >>> import netCDF4
   >>> times2 = netCDF4.num2date(times, units)

But it is more simple to use the following method:

.. code-block:: python

   >>> times2 = swath.get_datetimes()

.. warning::

  The conversion from time to datetime objects can be very long and must be
  avoided at all cost.


Slices can be used to get a subset of data:

.. code-block:: python

   >>> data = fd.read_values('solar_zenith_tn', slices={'row':slice(10,20), 'cell':slice(30, 40)})

A complete subset of the file can be obtained as follow:

.. code-block:: python

   >>> subset = swath.extract_subset(slices={'row': slice(10, 20), 'cell': slice(30, 40)})


The result is a new `Swath` object, but smaller. It contains the same list of
fields as the original parent object.

It can be saved into netCDF using a ``NCFile`` mapper:

.. code-block:: python
 
   >>> newncf = NCFile(url=subsetfname, mode=WRITE_NEW, ncformat='NETCDF4')
   >>> subset.save(newncf)


