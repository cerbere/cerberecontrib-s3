[build-system]
requires = ["poetry-core>=1.0.0", "poetry-dynamic-versioning"]
build-backend = "poetry_dynamic_versioning.backend"

[tool.poetry]
name = "cerberecontrib-s3"

version = "0.0.0"
description = "A Cerbere reader for EUMETSAT Sentinel-3 product"
license = "AOSI Approved :: GNU General Public License v3 (GPLv3)"
authors = ["Jean-François Piollé (Ifremer) <jean.francois.piolle@ifremer.fr>"]
maintainers = [
    "Jean-François Piollé (Ifremer) <jean.francois.piolle@ifremer.fr>",
]
readme = "README.md"
classifiers = [
    "Development Status :: 4 - Beta",
    "Environment :: Console",
    "Intended Audience :: Developers",
    "Intended Audience :: Science/Research",
    "License :: OSI Approved :: Apache Software License",
    "Operating System :: OS Independent",
    "Programming Language :: Python :: 3.8",
    "Programming Language :: Python :: 3.9",
    "Programming Language :: Python :: 3.10"
]
keywords = ["ocean", "observation", "data", "analysis", "sentinel-3",
    "slstr", "olci"]
exclude = ["tests"]

[[tool.poetry.source]]
name = "release"
url = "https://gitlab.ifremer.fr/api/v4/projects/1225/packages/pypi/simple/"
default = false
secondary = true

[tool.poetry-dynamic-versioning]
enable = true

vcs = "git"
style = "semver"
pattern = "^v?(?P<base>\\d+\\.\\d+\\.\\d+)(-?((?P<stage>[a-zA-Z]+)\\.?(?P<revision>\\d+)?))?$"

[tool.poetry.dependencies]
python = "^3.10"
lxml = "^4"
more_itertools = "^9"
attrs = "^24.2.0"
h5netcdf = "^1.3.0"
cerbere = "^3.1.2"
cerberecontrib_ghrsst = "^3.1.2"


[tool.poetry.dev-dependencies]
# execute commands before commit
pre-commit = "^2.15.0"
# static analysis
wemake-python-styleguide = "^0.16.0"
flake8 = "*"

# unit tests
pytest = "^6"
pytest-cov = "^2"
testfixtures = "^6"

[tool.black]
line-length = 120

[tool.pycln]
all = true

[tool.isort]
line_length = 120
multi_line_output = 3
include_trailing_comma = true
force_grid_wrap = 0
use_parentheses = true
ensure_newline_before_comments = true


[tool.poetry.plugins."cerbere.reader"]
S3SLRBTIR = 'cerberecontrib_s3.reader.s3slrbt:S3SLRBTIR'
S3SLRBTA = 'cerberecontrib_s3.reader.s3slrbt:S3SLRBTA'
S3SLRBTB = 'cerberecontrib_s3.reader.s3slrbt:S3SLRBTB'
S3SLWCT = 'cerberecontrib_s3.reader.s3slwct:S3SLWCT'
S3SLWCTCore = 'cerberecontrib_s3.reader.s3slwct:S3SLWCTCore'
S3SLWST = 'cerberecontrib_s3.reader.s3slwst:S3SLWST'
S3SLIST = 'cerberecontrib_s3.reader.s3slist:S3SLIST'
