import itertools
import os.path

import attrs
import more_itertools


def bucket(iterable, predicate):
    buckets = more_itertools.bucket(iterable, predicate)

    for name in buckets:
        yield name, buckets[name]


def groupby(iterable, *, key):
    sorted_ = sorted(iterable, key=key)

    return itertools.groupby(sorted_, key=key)


def file_extension(path):
    _, ext = os.path.splitext(path)
    return ext


@attrs.frozen
class chained:
    obj = attrs.field()

    def pipe(self, f, *args, **kargs):
        return self.__class__(f(self.obj, *args, **kargs))

    def __iter__(self):
        return iter(self.obj)


def flatten_group(group):
    yield group.name, group
    yield from itertools.chain.from_iterable(
        flatten_group(subgroup) for _, subgroup in group.children.items()
    )
