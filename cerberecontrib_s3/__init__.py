from importlib.metadata import version

try:
    __version__ = version("sentinel3_slstr")
except Exception:
    __version__ = "999"

