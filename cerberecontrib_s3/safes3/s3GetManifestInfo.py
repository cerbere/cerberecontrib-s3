#!/usr/bin/env python

__author__ = 'igort'

import sys
import getopt
import manifest



def usage():
    print('s3GetManifestInfo.py -i <path to S3 PDU> -c <config file>')
    print('Example:')
    print('     s3GetManifestInfo.py -i .../S3A_SL_1_RBT____20130707T153252_20130707T153752_20150217T183530_0299_158_182______SVL_O_NR_001.SEN3 -c s3tools/manifest_config.xml')


def main(argv):
    fn_s3_safe = ''
    config_file = None
    try:
        opts, _ = getopt.getopt(argv, "hi:c:", ["help", "ifile=", "config"])
        if not opts:
            usage()
            sys.exit(2)

    except getopt.GetoptError as err:
        print(err)
        usage()
        sys.exit(2)
    for opt, arg in opts:
        if opt == '-h':
            usage()
            sys.exit()
        elif opt in ("-i", "--ifile"):
            fn_s3_safe = arg
        elif opt in ("-c", "--config"):
            config_file = arg
        else:
            assert False, "unhandled option"

#    sys.exit(3)

    # run analysis
    mfs, _ = manifest.parse(fn_s3_safe, fn_config=config_file)

    # print to console
    manifest.print_data_to_console(mfs)


if __name__ == "__main__":
    main(sys.argv[1:])


