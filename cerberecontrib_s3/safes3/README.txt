Sentinel-3 manifest reader 

s3GetManifestInfo.py - command line utility to extract information from manifest file and print to console
                    s3GetManifestInfo.py -i <path to S3 PDU> -c <config file>
s3GetManifestInfo_test_slstr.sh - full example of reading manifest file using default xml configuration file
s3GetManifestInfo_test_slstr_config.sh - full example of reading manifest file using different xml configuration

s3tools/manifest.py - module containing functions for reading data from manifest file using xpath syntax stored in external XML configuration file
test - directory with the S3 product name and manifest file inside
