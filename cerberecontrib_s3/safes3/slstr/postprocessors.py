import itertools

import xarray as xr

from cerberecontrib_s3.utils.utils import bucket


def strip_name_suffix(components, *suffixes):
    *parts, name = components

    new_name = name
    for suffix in suffixes:
        new_name = new_name.removesuffix(suffix).rstrip("_")

    return *parts, new_name


def add_prefix(prefix):
    def execute(ds):
        return ds.rename_vars(
            {name: f"{prefix}_{name}" for name in ds.data_vars.keys()}
        ).rename_dims(
            {dim: f"{prefix}_{dim}" for dim in ['uncertainties']
             if dim in ds.dims}
        )

    return execute


def squeeze(ds):
    return ds.squeeze()


def lowercase(ds):
    return ds.rename(
        {name: name.lower() for name in ds.variables if name != name.lower()}
    )


def remove_suffix(ds, name):
    translations = {k: k.removesuffix(f"_{name}") for k in ds.variables.keys()}
    return ds.rename_vars(translations)


def as_coords(ds, condition=None, coords=None):
    if not condition:
        return ds

    if coords is None:
        coords = list(ds.variables.keys())

    return ds.set_coords(coords)


def split_time(tree, channels='ir_thermal'):
    prefixes = ["oblique", "nadir"]
    time = tree[f'/{channels}/nadir/time']
    # time is mixed, but the attributes are for nadir
    # so it's better to drop the attributes entirely
    time.attrs = {}

    def strip_prefix_and_suffix(name):
        new_name = name
        for prefix in prefixes:
            new_name = new_name.removeprefix(prefix)

        for suffix in ['_i', '_a', '_b']:
            if new_name.endswith(suffix):
                return new_name.removesuffix(suffix).strip("_")

        return new_name

    def classify(name):
        for prefix in prefixes:
            if name.startswith(prefix):
                yield prefix, name
                return
        yield from itertools.zip_longest(prefixes, [], fillvalue=name)

    classified = list(
        itertools.chain.from_iterable(classify(name) for name in time.data_vars.keys())
    )
    separated = {
        name: [name for _, name in group]
        for name, group in bucket(classified, lambda x: x[0])
    }
    new_nodes = {
        f"/{channels}/{node_name}/time": xr.DataTree(
            dataset=(
                time.ds[variable_names].rename(
                    {name: strip_prefix_and_suffix(name) for name in variable_names}
                )
            )
        )
        for node_name, variable_names in separated.items()
    }

    new_tree = tree.copy()
    for path, node in new_nodes.items():
        new_tree[path] = node

    return new_tree
