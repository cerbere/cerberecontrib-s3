import os
import pathlib
import re
import warnings
from typing import Union, Dict

import attrs
import fsspec
import xarray as xr

from cerberecontrib_s3.safes3 import manifest
from cerberecontrib_s3.safes3.slstr import codes, postprocessors
from cerberecontrib_s3.utils import utils

fname_re = re.compile(
    r"(?P<name>[A-Za-z0-9_]+)_(?P<grid>[a-z])(?P<view>[a-z])\.(?P<ext>.+)"
)


@attrs.frozen
class WCTFilename:
    """The filename of a WCT file split into components

    Attributes
    ----------
    filepath
        The file path
    filename
        The original filename
    name
        The name of the component
    grid
        The name of the grid
    view
        The name of the view
    suffix
        The combined grid and view codes
    """

    filepath: str
    filename: str
    extension: str

    name: str
    grid: codes.Grid
    view: codes.View

    suffix: str

    @classmethod
    def from_path(cls, filepath: str):
        filename = os.path.basename(filepath)

        match = fname_re.match(filename)
        if match is None:
            raise ValueError(f"cannot parse filename: {filename}")

        parts = match.groupdict()
        kwargs = {
            "filepath": filepath,
            "filename": filename,
            "extension": parts["ext"],
            "name": parts["name"].lower(),
            "grid": codes.lookup_code(codes.grids, parts["grid"]),
            "view": codes.lookup_code(codes.views, parts["view"]),
            "suffix": parts["grid"] + parts["view"],
        }

        return cls(**kwargs)


def extract_path_components(key):
    return key.grid.name, key.view.name, key.name


def open_wct(path: Union[str, pathlib.Path], **open_kwargs):
    """open wct files

    Parameters
    ----------
    path
        The path or url of the WCT dataset
    """
    url = path if isinstance(path, str) else os.fspath(path)
    fs, _, _ = fsspec.get_fs_token_paths(url)

    if not fs.isdir(url):
        raise ValueError(f"not a WCT store: {url} is not a directory")

    all_paths = fs.ls(url)

    # groupby the file extension to filter out the manifest
    paths_by_extension = {
        ext: list(paths)
        for ext, paths in utils.bucket(
            all_paths, lambda p: utils.file_extension(p).lstrip(".")
        )
    }

    # group the netcdf files by components and open them
    nc_paths = paths_by_extension["nc"]
    parsed_filenames = [WCTFilename.from_path(path) for path in nc_paths]

    grouped = {
        utils.chained(extract_path_components(wct)).pipe(
            postprocessors.strip_name_suffix, "sst"
        ): wct
        for wct in parsed_filenames
    }

    dss = {
        "/".join(node_components): (
            xr.open_dataset(fs.open(wct.filepath), **open_kwargs)
            .pipe(
                postprocessors.as_coords,
                wct.name in {"cartesian", "geodetic", "indices"},
            )
            .pipe(postprocessors.squeeze)
            .pipe(postprocessors.remove_suffix, wct.suffix)
            .pipe(postprocessors.lowercase)
        )
        for node_components, wct in grouped.items()
    }

    # put everything into the datatree
    tree = xr.DataTree.from_dict(dss).pipe(postprocessors.split_time)
    tree.encoding["source"] = url

    # parse the manifest file
    xml_paths = paths_by_extension.get("xml", [])
    if len(xml_paths) > 1:
        raise IOError(
            f'There should be only one manifest file; found: {xml_paths}')
    elif len(xml_paths) == 0:
        warnings.warn(
            "Missing manifest file. You won't get all global attributes")
    else:
        tree.attrs.update(add_manifest_info(xml_paths[0]))

    return tree


def add_manifest_info(path: Union[str, pathlib.Path]) -> Dict:
    """Add more global attributes from manifest content"""
    url = path if isinstance(path, str) else os.fspath(path)

    content, _ = manifest.parse(pathlib.Path(path).parent)

    return content

