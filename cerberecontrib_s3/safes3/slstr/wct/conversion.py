import itertools
from collections import Counter

import xarray as xr

from cerberecontrib_s3.safes3.slstr import postprocessors
from cerberecontrib_s3.utils.utils import flatten_group


def merge_subgroups(node, views=["oblique", "nadir"]):

    merged_groups = {
        name: xr.merge(
            [
                subgroup.to_dataset().pipe(
                    lambda ds: postprocessors.add_prefix(name)(ds)
                    if name not in {"time", "flags", "geodetic", "cartesian", "indices"}
                    else ds
                )
                for name, subgroup in flatten_group(group)
            ],
            combine_attrs="drop_conflicts",
        )
        for name, group in node.children.items()
    }
    return xr.DataTree.from_dict(
        {
            node_path: ds
            for node_path, ds in itertools.chain(
                [("/", node.to_dataset())], merged_groups.items()
            )
        }
    )


def merge_meteorology(tie_point):
    """join together view agnostic with each of nadir and oblique"""
    agnostic = tie_point["agnostic"]
    common = xr.merge(
        [node.ds for name, node in agnostic.children.items()],
        combine_attrs="drop_conflicts",
    )

    new_nodes = {
        name: xr.merge(
            [group.to_dataset() for _, group in flatten_group(node)],
            combine_attrs="drop_conflicts",
        )
        for name, node in tie_point.children.items()
        if name != "agnostic"
    }

    return xr.DataTree.from_dict(
        {
            name: xr.merge([common, ds], combine_attrs="drop_conflicts")
            for name, ds in new_nodes.items()
        }
    )


def calculate_time(ds):
    def _calculate(first_min_ts, scan, first_scan, scansync, pixel, pixsync):
        return first_min_ts.astype('timedelta64[us]') + (scan - first_scan) *\
            scansync + pixel * pixsync

    def to_timedelta64(variable):
        units = variable.attrs["units"]

        return variable.astype(f"timedelta64[{units}]")

    return _calculate(
        first_min_ts=ds.minimal_ts,
        scan=ds.scan,
        first_scan=ds.first_scan,
        scansync=to_timedelta64(ds.scansync),
        pixel=ds.pixel,
        pixsync=to_timedelta64(ds.pixsync),
    ).rename("time")

#@profile
def assign_times(node_or_ds):
    if not isinstance(node_or_ds, xr.Dataset) or len(node_or_ds) == 0:
        return node_or_ds

    ds = node_or_ds

    ds = ds.assign_coords(time=lambda ds: calculate_time(ds)).drop_vars(
        [
            "pixsync",
            "scansync",
            "pixel",
            "scan",
            "first_pixel",
            "first_scan",
            "last_scan",
            "minimal_ts",
            "maximal_ts",
        ]
    )

    ds.time.attrs['long_name'] = 'measurement time'

    return ds

#@profile
def combine_subgroups(tree, channels='ir_thermal'):
    subgroups = merge_subgroups(
        tree[f'/{channels}']).map_over_datasets(assign_times)
    meteorology = merge_meteorology(tree["/tie_point"])

    return xr.DataTree.from_dict(
        {
            f'/{channels}': subgroups,
            "/tie_point": meteorology,
        }
    )

#@profile
def pad_oblique(node):
    nadir_offset = node["nadir"].attrs["track_offset"]
    oblique_offset = node["oblique"].attrs["track_offset"]

    left_pad = nadir_offset - oblique_offset
    right_pad = node["nadir"].sizes["columns"] - (
        left_pad + node["oblique"].sizes["columns"]
    )

    padded = node["oblique"].dataset.pad(
        columns=(left_pad, right_pad),
        mode="constant",
        constant_value=xr.core.dtypes.NA,
        keep_attrs=True,
    )

    new_node = node.copy()
    new_node["oblique"] = padded

    return new_node

#@profile
def drop_common_variables(tree):
    """drop duplicate names from meteorology and oblique"""
    counter = Counter(
        itertools.chain.from_iterable(
            node.variables for node in tree.children.values())
    )
    to_drop = [name for name, count in counter.items() if count >= 2]

    pruned_nodes = {}
    for name, node in tree.children.items():
        if name != 'nadir':
            pruned_nodes[name] = xr.DataTree(
                node.dataset.drop_vars(to_drop, errors="ignore"), name=name)

    return tree.assign(pruned_nodes)
