import re

import xarray as xr

from cerberecontrib_s3.safes3.interpolation import (
    interpolate_meteorology_cerbere,
    interpolate_meteorology_scipy,
    interpolate_meteorology_xesmf,
)
from cerberecontrib_s3.safes3.slstr.wct.conversion import combine_subgroups, drop_common_variables, pad_oblique


class Sentinel3SlstrWct:
    timestamp_re = r"[0-9]{8}T[0-9]{6}"
    pattern = re.compile(
        rf"S3A_SL_2_WCT____{timestamp_re}_{timestamp_re}_{timestamp_re}_[0-9_]+_MAR_O_NR_003.SEN3"
    )

    def postprocess(tree, interpolation_method="xesmf"):
        interpolation_methods = {
            "xesmf": interpolate_meteorology_xesmf,
            "scipy": interpolate_meteorology_scipy,
            "cerbere": interpolate_meteorology_cerbere,
        }
        interpolate_meteorology = interpolation_methods.get(interpolation_method)
        if interpolate_meteorology is None:
            raise ValueError(
                f"unknown interpolation method ({interpolation_method}),"
                f" choose one of {sorted(interpolation_methods)}"
            )

        return (
            tree.pipe(combine_subgroups)
            .pipe(
                lambda dt: interpolate_meteorology(dt["/ir_thermal"], dt["/tie_point"])
            )
            .pipe(pad_oblique)
            .pipe(drop_common_variables)
            .pipe(lambda dt: xr.merge([node.ds for node in dt.subtree]))
        )
