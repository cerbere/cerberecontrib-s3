import attrs

@attrs.frozen
class Grid:
    """Grid specification

    Attributes
    ----------
    abbrev : str
        The abbreviation used in the file name
    name : str
        The name used in the tree
    description : str
        The description of the grid.
    """

    abbrev: str
    name: str
    description: str


@attrs.frozen
class View:
    """View specification

    Attributes
    ----------
    abbrev : str
        The abbreviation used in the file name
    name : str
        The name used in the tree
    description : str
        The description of the view
    """

    abbrev: str
    name: str
    description: str


grids = {
    "i": Grid(abbrev="i", name="ir_thermal", description="1km thermal infra-red grid"),
    "f": Grid(abbrev="f", name="f1", description="1km dedicated F1 grid"),
    "a": Grid(
        abbrev="a", name="a_stripe", description="500m visible and SWIR A-stripe grid"
    ),
    "b": Grid(
        abbrev="b", name="b_stripe", description="500m visible and SWIR B-stripe grid"
    ),
    "c": Grid(abbrev="c", name="tdi", description="500m TDI grid"),
    "t": Grid(abbrev="t", name="tie_point", description="tie point grid"),
}

views = {
    "n": View(abbrev="n", name="nadir", description="nadir view"),
    "o": View(abbrev="o", name="oblique", description="oblique view"),
    "x": View(abbrev="x", name="agnostic", description="view agnostic"),
}


def lookup_code(mapping, code):
    translated = mapping.get(code)
    if translated is None:
        raise ValueError(
            f"unknown code: {code}. Known values: [{', '.join(sorted(mapping))}]"
        )

    return translated
