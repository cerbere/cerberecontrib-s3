import os
import pathlib
import re

import attrs
import fsspec
import xarray as xr

from cerberecontrib_s3.safes3.slstr import codes, postprocessors
from cerberecontrib_s3.utils import utils


@attrs.frozen
class RBTFilename:
    """The filename of a RBT file split into components

    Attributes
    ----------
    filepath
        The file path
    filename
        The original filename
    name
        The name of the component
    grid
        The name of the grid
    view
        The name of the view
    suffix
        The combined grid and view codes
    """

    filepath: str
    filename: str
    extension: str

    name: str
    grid: codes.Grid
    view: codes.View

    suffix: str

    fname_re = re.compile(
        r"(?P<name>[A-Za-z0-9_]+)_(?P<grid>[a-z])(?P<view>[a-z])\.(?P<ext>.+)"
    )

    @classmethod
    def from_path(cls, filepath: str):
        filename = os.path.basename(filepath)
        name, ext = os.path.splitext(filename)

        if filename.startswith("viscal"):
            kwargs = {
                "filepath": filepath,
                "filename": filename,
                "extension": ext,
                "name": name,
                "grid": None,
                "view": None,
                "suffix": None,
            }
        else:
            match = cls.fname_re.fullmatch(filename)
            if match is None:
                raise ValueError(f"cannot parse filename: {filename}")

            parts = match.groupdict()
            kwargs = {
                "filepath": filepath,
                "filename": filename,
                "extension": parts["ext"],
                "name": parts["name"].lower(),
                "grid": codes.lookup_code(codes.grids, parts["grid"]),
                "view": codes.lookup_code(codes.views, parts["view"]),
                "suffix": parts["grid"] + parts["view"],
            }

        return cls(**kwargs)


def extract_path_components(key):
    if key.name == "viscal":
        return (key.name,)

    return key.grid.name, key.view.name, key.name


def open_rbt(path: str | pathlib.Path,
             channels: str = 'ir_thermal',
             **open_kwargs):
    """open rbt files

    Parameters
    ----------
    path
        The path or url of the RBT dataset
    """
    url = path if isinstance(path, str) else os.fspath(path)
    fs, _, _ = fsspec.get_fs_token_paths(url)

    if not fs.isdir(url):
        raise ValueError(f"not a RBT store: {url} is not a directory")

    all_paths = fs.ls(url)

    # groupby the file extension to filter out the manifest
    paths_by_extension = {
        ext: list(paths)
        for ext, paths in utils.bucket(
            all_paths, lambda p: utils.file_extension(p).lstrip(".")
        )
    }

    # group the netcdf files by components and open them
    nc_paths = paths_by_extension["nc"]
    parsed_filenames = [RBTFilename.from_path(path) for path in nc_paths]

    grouped = {
        utils.chained(extract_path_components(rbt)).pipe(
            postprocessors.strip_name_suffix, "sst"
        ): rbt
        for rbt in parsed_filenames
    }

    dss = {
        "/".join(node_components): (
            xr.open_dataset(fs.open(rbt.filepath), **open_kwargs)
            .pipe(
                postprocessors.as_coords,
                rbt.name in {"cartesian", "geodetic", "indices"},
            )
            .pipe(postprocessors.squeeze)
            .pipe(postprocessors.remove_suffix, rbt.suffix)
            .pipe(postprocessors.lowercase)
        )
        for node_components, rbt in grouped.items()
    }

    # put everything into the datatree
    tree = xr.DataTree.from_dict(dss).pipe(
        postprocessors.split_time, channels=channels)

    return tree

