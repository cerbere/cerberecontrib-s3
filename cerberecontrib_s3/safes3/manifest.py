""" Module for reading Sentinel 3 manifest files.
Reads information from S3 manifest xml (xfdumanifest.xml) file and stores element values inside Dictionary (Ordered) object.
Input XML configuration file (Manifest config file) is used to define what information should be retrieved from the manifest file.
It uses xpath syntax to define path to element and short description that is used as key in the dictionary.
Example of access to platform family name:
    <xpath name="platform_familyName">/xfdu:XFDU/metadataSection/metadataObject[@ID="platform"]/metadataWrap/xmlData/sentinel-safe:platform/sentinel-safe:familyName</xpath>
    and the key/value output:
    platform_familyName : Sentinel-3
Input XML config file is divided in sections for easier grouping:
    <xpaths ns="slstr">
Default xml config file has the following name: <manifest_config.xml> and is located in the directory of this module.

"""
import os
from collections import OrderedDict
import ast
import xml.etree.ElementTree as etree
import dateutil.parser as dateparser
import urllib.parse

import fsspec


__author__ = 'igort'


# namespaces in S3 SAFE manifest xml
MFS_NAMESPACES = {'xfdu': 'urn:ccsds:schema:xfdu:1',
                  'sentinel-safe': 'http://www.esa.int/safe/sentinel/1.1',
                  'gml': 'http://www.opengis.net/gml',
                  'sentinel3': 'http://www.esa.int/safe/sentinel/sentinel-3/1.0',
                  'slstr': 'http://www.esa.int/safe/sentinel/sentinel-3/slstr/1.0',
                  'olci': 'http://www.esa.int/safe/sentinel/sentinel-3/olci/1.0',
                  'sral': 'http://www.esa.int/safe/sentinel/sentinel-3/sral/1.0'}

# path to default configuration manifest file where xpaths are defined
FN_MANIFEST_CONFIG_DEFAULT = os.path.join(os.path.dirname(__file__),
                                          'manifest_config.xml')

# name of the S3 SAFE manifest files
FN_MANIFEST_XML = 'xfdumanifest.xml'

# xpath to get instrument abbrev - needed to get instrument specific
# xpaths from config file
XPATH_INSTRUMENT_ABBREV = './/*[@ID="platform"]/metadataWrap/xmlData/sentinel-safe:platform/sentinel-safe:instrument/sentinel-safe:familyName/@abbreviation'
#XPATH_INSTRUMENT_ABBREV = './/*[@ID="platform"]/metadataWrap/xmlData/sentinel-safe:platform/sentinel-safe:instrument/sentinel-safe:familyName'


# list of keys to exclude from printing to the screen/file
PRINT_EXCLUDE_KEYS = ['footPrint_posList', 's3_safe']


def get_manifest_tree(fn):
    if str(fn).startswith('s3://') or str(fn).startswith('https://'):
        fn_xml = urllib.parse.urljoin(fn, FN_MANIFEST_XML)
        protocol = urllib.parse.urlparse(str(fn)).scheme
        fs = fsspec.filesystem(protocol)
        fb = fs.open(fn_xml, mode='r')

        return etree.parse(fb)
    else:
        fn_xml = os.path.join(fn, FN_MANIFEST_XML)

        return etree.parse(fn_xml)


def parse(fn_s3, tree=None, namespaces=MFS_NAMESPACES, fn_config=None):
    """ parse manifest file - main function
    :param fn_s3: s3 filename
    :param tree: tree from xml
    :param dict_namespaces:  namespaces
    :param fn_config: config xml file
    :return: out - dictioniary of xpaths/values pairs
    """
    if tree is None:
        tree = get_manifest_tree(fn_s3)

    # get instrument
    instr = get_instrument_name(fn_s3, tree=tree)

    if fn_config is None:
        # get default config file
        fn_config = FN_MANIFEST_CONFIG_DEFAULT

    # get xpaths from config file
    xpath_arr = _get_xpaths_from_config(
        fn_config, namespaces=['default', instr])

    # get data from manifest based on xpaths
    out = get_from_manifest(fn_s3, xpath_arr, tree=tree, namespaces=namespaces)
    return out, tree


def _get_xpaths_from_config(fn_config, namespaces=['default']):
    """ get xpath list from xml config file
    :param fn_config:
    :param ns:
    :return:
    """
    xml_cfg = etree.parse(fn_config)
    out = []
    for namespace in namespaces:
        #xpath = "/manifest_xpaths/xpaths[@ns='{0}']/".format(namespace)
        xpath = "*[@ns='{0}']/".format(namespace)
        t = xml_cfg.findall(xpath)
        for xp in t:
            out.append([xp.attrib['name'], xp.text])
    return out


def _get_from_manifest(fn, xpath, dict_namespaces, tree=None):
    """ get single xpath value from manifest file
    :param fn: file name
    :param xpath: xpath
    :param dict_namespaces: dictionary of namespaces
    :param tree: manifest xml tree
    :return:
    """

    if tree is None:
        tree = get_manifest_tree(fn)

    #out = tree.xpath(xpath, namespaces=dict_namespaces)
    for namespc in dict_namespaces:
        etree.register_namespace(namespc, dict_namespaces[namespc])

    # is it an attribute?
    if '/@' in xpath:
        xpath2, attr = xpath.split('/@')
        out = tree.findall(xpath2, dict_namespaces)
        for i, item in enumerate(out):
            out[i] = item.attrib[attr]
    else:
        out = tree.findall(xpath, dict_namespaces)

    return out


def get_from_manifest(fn_s3, xpath_arr, namespaces=MFS_NAMESPACES, tree=None):
    """ get xpaths values from manifest file
    :param fn: file name
    :param xpath_arr: xpath_arr - list of xpath values
    :param dict_namespaces: dictionary of namespaces
    :param tree: manifest xml tree
    :return:
    """

    if tree is None:
        tree = get_tree_from_manifest(fn_s3)

    out = OrderedDict()
    # get using XPath
    out['s3_safe'] = OrderedDict()
    out['s3_safe']['value'] = fn_s3
    for _, xpath in enumerate(xpath_arr):
        xpath_value = xpath[1]
        xpath_desc = xpath[0]

        # discard unexisting attributes
        if xpath_value == []:
            out.pop(xpath_desc)

        tmp = _get_from_manifest(fn_s3, xpath_value, namespaces, tree=tree)
        out[xpath_desc] = OrderedDict()
        out[xpath_desc]['xPath'] = xpath_value
        out[xpath_desc]['name'] = xpath_desc
        if xpath_desc == 'footPrint_posList':
            t = list(map(float, tmp[0].text.split()))
            val_tmp = [t[1::2], t[0::2]]
        else:
            if not tmp:
                val_tmp = tmp
            else:
                val_tmp = tmp[0]
        if isinstance(val_tmp, etree.Element):
            # it is element object
            val = val_tmp.text
        else:
            # otherwise is attribute
            val = val_tmp

        # convert to proper type when possible
        if any([_ in xpath_value for _ in ["Time", "start", "stop"]]):
            val = dateparser.parse(val)
        else:
            try:
                val = ast.literal_eval(val)
            except:
                pass

        out[xpath_desc]['value'] = val
    return out


def get_tree_from_manifest(fn_s3):
    """ Load xml manifest tree
    :param fn_s3: path to S3 product (directory)
    :return:
    """
    fn_xml = os.path.join(fn_s3, FN_MANIFEST_XML)
    tree = etree.parse(fn_xml)
    return tree


def get_instrument_name(fn_s3, tree=None, namespaces=MFS_NAMESPACES):
    """ Get instrument name from manifest file
    :param fn_s3: path to S3 product (directory)
    :param tree: xml manifest tree
    :param namespaces: namespaces in manifest file
    :return: instrument name
    """
    if tree is None:
        tree = get_tree_from_manifest(fn_s3)
    instr = _get_from_manifest(fn_s3, XPATH_INSTRUMENT_ABBREV, namespaces,
                               tree=tree)
    return instr[0].lower()


def print_data_to_console(mfs):
    """ Print values to console
    :param mfs: manifest dictionary containing keys/values
    :            (xpath_desc/xpath_value)
    :return:
    """
    # print to console
    fn_path, fn_root = os.path.split(mfs['s3_safe']['value'])
    print("************************************")
    print("S3 name: {0}".format(fn_root))
    print("S3 path: {0}".format(fn_path))
    for key in mfs.keys():
        if key not in PRINT_EXCLUDE_KEYS:
            print("{0:50s}: {1}".format(key, mfs[key]['value']))


if __name__ == '__main__':

    fn_s3_safe1 = 'test/S3A_SL_1_RBT____20130707T224317_20130707T224616_20150806T181712_0179_015_286_3867_MAR_O_NR_001.SEN3'
    mfs1 = parse(fn_s3_safe1)
    print_data_to_console(mfs1)
