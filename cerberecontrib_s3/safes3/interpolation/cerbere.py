import dask
import numpy as np
import xarray as xr

from cerberecontrib_s3.safes3.interpolation.dask import apply_dask_blockwise


def interpolate_bilinear_cerbere(x_s, y_s, data, x_t, y_t, neighbours):
    """bilinear interpolation on overlapping regions

    Parameters
    ----------
    x_s, y_s : array-like
        1D source coordinates
    data : array-like
        data to interpolate. The interpolated dimensions must be first (all but the last two dimensions will be flattened).
    x_t, y_t : array-like
        target coordinates

    Returns
    -------
    interpolated : array-like
        interpolated data
    """

    def nearest_interpolation(target_coord, w1, w2, interpolated, v1, v2):
        indices = np.where(~np.isnan(target_coord[None, ...]) & np.isnan(interpolated))
        if indices[0].size == 0:
            return interpolated

        near_first = abs(w1[indices[1], indices[2]]) >= abs(w2[indices[1], indices[2]])
        indices_first = tuple(indices_[near_first] for indices_ in indices)
        indices_second = tuple(indices_[~near_first] for indices_ in indices)

        new_values = interpolated.copy()
        new_values[indices_first] = v1[indices_first]
        new_values[indices_second] = v2[indices_second]

        return new_values

    source_shape = data.shape[:2]
    reshaped = data.reshape(*source_shape, -1)
    if data.ndim > 2:
        data_ = np.moveaxis(reshaped, -1, 0)
    else:
        data_ = np.squeeze(reshaped)

    data_ = np.ascontiguousarray(data_)

    # find the neighbor coordinates
    left, right, bottom, top = neighbours

    #left = np.clip(np.searchsorted(x_s, x_t) - 1, 0, x_s.size - 2)
    #right = left + 1
    #bottom = np.clip(np.searchsorted(y_s, y_t) - 1, 0, y_s.size - 2)
    #top = bottom + 1

    # interpolate along x
    weights_left = (x_t - x_s[right]) / (x_s[right] - x_s[left])
    weights_right = (x_s[left] - x_t) / (x_s[right] - x_s[left])

    bottom_values = (
        weights_left[None, ...] * data_[..., bottom, left]
        + weights_right * data_[..., bottom, right]
    )
    top_values = (
        weights_left[None, ...] * data_[..., top, left]
        + weights_right * data_[..., top, right]
    )

    bottom_values_ = nearest_interpolation(
        x_t,
        weights_left,
        weights_right,
        bottom_values,
        data_[..., bottom, left],
        data_[..., bottom, right],
    )
    top_values_ = nearest_interpolation(
        x_t,
        weights_left,
        weights_right,
        top_values,
        data_[..., top, left],
        data_[..., top, right],
    )

    # interpolate along y
    weights_bottom = (y_t - y_s[top]) / (y_s[top] - y_s[bottom])
    weights_top = (y_s[bottom] - y_t) / (y_s[top] - y_s[bottom])
    values = (
        weights_bottom[None, ...] * bottom_values_
        + weights_top[None, ...] * top_values_
    )

    values_ = nearest_interpolation(
        y_t, weights_bottom, weights_top, values, bottom_values, top_values
    )

    target_shape = x_t.shape
    interpolated_shape = data.shape[2:] + target_shape
    reshaped = np.squeeze(values_).reshape(*interpolated_shape)
    if data.ndim > 2:
        reshaped = np.moveaxis(reshaped, 0, -1)
    return reshaped


class BilinearCerbereRegridder:
    order = ["rows", "columns"]

    def __init__(self, source, target):
        x_s = source["x"].isel(rows=0)
        y_s = source["y"].isel(columns=0)

        self.target = target
        target_points = self.target.transpose(*self.order)
        self.x_t = target_points["x"]
        self.y_t = target_points["y"]

        self.x_s, self.y_s = dask.compute(x_s, y_s)

    def regrid_dataset(self, ds, *, method="linear", **kwargs):
        #@profile
        def _regrid_variable(x_s, y_s, var, neighbours):
            if not set(var.dims).issuperset(self.order):
                return var

            data = var.transpose(*self.order, ...)

            regridded_values = apply_dask_blockwise(
                interpolate_bilinear_cerbere,
                x_s.data,
                y_s.data,
                data.data,
                self.x_t.data,
                self.y_t.data,
                neighbours
            )
            regridded = xr.Variable(data=regridded_values, dims=data.dims)

            return regridded

        extent_x = slice(
            float(np.maximum(self.x_s.min(), self.x_t.min())),
            float(np.minimum(self.x_s.max(), self.x_t.max())),
        )
        extent_y = slice(
            float(np.maximum(self.y_s.min(), self.y_t.min())),
            float(np.minimum(self.y_s.max(), self.y_t.max())),
        )
        trimmed_ds = (
            ds.assign_coords({"x": self.x_s.variable, "y": self.y_s.variable})
            .swap_dims({"rows": "y", "columns": "x"})
            .sortby(
                ["x", "y"], ascending=True
            )  # make sure the indices are monotonic ascending
            .sel(x=extent_x, y=extent_y)
            .swap_dims({"y": "rows", "x": "columns"})
        )

        x_s = trimmed_ds["x"]
        y_s = trimmed_ds["y"]

        left = np.clip(
            np.searchsorted(x_s.data, self.x_t) - 1, 0, x_s.size - 2)
        right = left + 1
        bottom = np.clip(
            np.searchsorted(y_s.data, self.y_t) - 1, 0, y_s.size - 2)
        top = bottom + 1

        data_vars = {
            name: _regrid_variable(x_s, y_s, arr.variable,
                                   (left, right, bottom, top))
            for name, arr in trimmed_ds.data_vars.items()
        }
        return xr.Dataset(data_vars=data_vars, coords=self.target.coords)


#@profile
def interpolate_meteorology_cerbere(ir_thermal, meteorology):
    source = meteorology["nadir"].ds
    target = ir_thermal["nadir"].ds

    regridder = BilinearCerbereRegridder(
        source[["x", "y"]],
        target[["x", "y"]],
    )
    ds = regridder.regrid_dataset(source)

    result = ir_thermal.copy()
    result["meteorology"] = xr.DataTree(dataset=ds)

    return result
