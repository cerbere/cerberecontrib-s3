import xarray as xr


def interpolate_meteorology_xesmf(ir_thermal, meteorology):
    import xesmf

    source = meteorology["nadir"].ds
    target = ir_thermal["nadir"].ds

    regridder = xesmf.Regridder(
        source[["latitude", "longitude"]],
        target[["latitude", "longitude"]],
        method="bilinear",
        extrap_method="nearest_s2d",
        unmapped_to_nan=True,
    )
    ds = regridder.regrid_dataset(source, keep_attrs=True)

    result = ir_thermal.copy()
    result["meteorology"] = xr.DataTree(data=ds)

    return result
