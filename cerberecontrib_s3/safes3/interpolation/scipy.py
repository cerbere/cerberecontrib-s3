import dask
import numpy as np
import xarray as xr

from cerberecontrib_s3.safes3.interpolation.dask import apply_dask_blockwise


def interpolate_scipy(x_s, y_s, data, x_t, y_t, *, method, **kwargs):
    from scipy.interpolate import RegularGridInterpolator

    # source_points needs to contain C-contiguous arrays (that's a bug in the most recent scipy)
    source_points = (np.ascontiguousarray(y_s), np.ascontiguousarray(x_s))

    interpolator = RegularGridInterpolator(
        points=source_points, values=data, method=method, **kwargs
    )

    stacked = np.stack([y_t, x_t], axis=-1)
    target_points = stacked.reshape(-1, 2)

    interpolated = interpolator(target_points)
    new_shape = stacked.shape[:-1] + data.shape[2:]
    reshaped = interpolated.reshape(new_shape)

    return reshaped


class RegularGridRegridder:
    order = ["rows", "columns"]

    def __init__(self, source, target):
        x_s = source["x"].isel(rows=0)
        y_s = source["y"].isel(columns=0)

        self.target = target
        target_points = self.target.transpose(*self.order)
        self.x_t = target_points["x"]
        self.y_t = target_points["y"]

        self.x_s, self.y_s = dask.compute(x_s, y_s)

    def regrid_dataset(self, ds, *, method="linear", **kwargs):
        def _regrid_variable(var):
            if not set(var.dims).issuperset(self.order):
                return var

            dims_ = [dim for dim in var.dims if dim not in self.order]
            data = var.transpose(*self.order, ...).astype("float64")

            interpolated = apply_dask_blockwise(
                interpolate_scipy,
                self.x_s.data,
                self.y_s.data,
                data.data,
                self.x_t.data,
                self.y_t.data,
                method=method,
                **kwargs,
            )

            return xr.Variable(self.order + dims_, interpolated)

        data_vars = {
            name: _regrid_variable(arr.variable) for name, arr in ds.data_vars.items()
        }
        result = xr.Dataset(data_vars=data_vars, coords=self.target.coords)

        return result


def interpolate_meteorology_scipy(ir_thermal, meteorology):
    source = meteorology["nadir"].ds
    target = ir_thermal["nadir"].ds

    regridder = RegularGridRegridder(
        source[["x", "y"]],
        target[["x", "y"]],
    )
    ds = regridder.regrid_dataset(
        source, method="linear", fill_value=np.nan, bounds_error=False
    )

    result = ir_thermal.copy()
    result["meteorology"] = xr.DataTree(dataset=ds)

    return result
