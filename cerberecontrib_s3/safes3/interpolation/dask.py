import dask
import dask.array as da
import numpy as np


def apply_dask_blockwise(f, x_s, y_s, data, x_t, y_t, neighbours, **kwargs):
    if not any(dask.is_dask_collection(_) for _ in (x_s, y_s, data, x_t, y_t)):
        return f(x_s, y_s, data, x_t, y_t, neighbours, **kwargs)

    #if data.chunks[:2] != tuple((size,) for size in data.shape[:2]):
    #    raise ValueError("cannot apply to more than a single source chunk")

    meta = np.array((), dtype=data.dtype)

    available_dims = "abcdefgh"
    other_dims = available_dims[: data.ndim - 2]
    input_scheme = "yx" + other_dims
    output_scheme = "vw" + other_dims

    return da.blockwise(
        f,
        output_scheme,
        x_s,
        "x",
        y_s,
        "y",
        data,
        input_scheme,
        x_t,
        "vw",
        y_t,
        "vw",
        meta=meta,
        concatenate=True,
        neighbours=neighbours,
        **kwargs,
    )
