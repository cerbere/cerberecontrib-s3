import re
import typing as T
from pathlib import Path

import numpy as np
import xarray as xr
from cerbere.reader.basereader import BaseReader

from cerberecontrib_s3.safes3.slstr import rbt


class S3SLRBT(BaseReader):

    timestamp_re = r"[0-9]{8}T[0-9]{6}"
    pattern: str = re.compile(
       rf"S3A_SL_1_RBT____{timestamp_re}_{timestamp_re}_{timestamp_re}.*SEN3")
    engine: str = "h5netcdf"
    description: str = ("Read Sentinel-3 SLSTR RBT SAFE container in Xarray "
                        "as a single Dataset")
    url: str = "https://link_to/your_backend/documentation"

    @classmethod
    #@profile
    def open_dataset(cls,
                     filename_or_obj: T.Union[str, Path],
                     channels='ir_thermal',
                     interpolation_method='cerbere',
                     model_vars: bool = False,
                     **kwargs):
        """open RBT files

        Parameters
        ----------
        path
            The path or url of the RBT dataset
        channels:
            channel type among: ir_thermal, a_stripe, b_stripe
        """
        tree = rbt.open_rbt(
            filename_or_obj,
            #chunks={'columns': 750, 'rows': 600},
            chunks={'columns': -1, 'rows': -1},
            drop_variables=['t_bound'],
            channels=channels,
            **kwargs)

        # save global attributes
        attrs = tree[f'{channels}/nadir/geodetic'].ds.attrs

        if model_vars:
            raise ValueError('Model data interpolation not supported by this '
                             'reader')
        else:
            ds = (
                tree
                .pipe(rbt.combine_subgroups, channels=channels)
                .pipe(lambda dt: dt[f'/{channels}'])
                .pipe(rbt.pad_oblique)
                .pipe(rbt.drop_common_variables)
                .pipe(lambda dt: xr.merge([node.ds for node in dt.subtree]))
            )

        # convert time to datetime64
        ds = ds.assign_coords(
            time=(np.datetime64('2000-01-01T00:00:00') + ds.time).astype(
                'datetime64[ns]'))

        # fix some issues
        if 't_bound' in ds:
            # malformed time unit
            ds.t_bound.attrs.pop('units')
            ds.t_bound.encoding['units'] = ds.t_forecast.attrs['units']

        # can not handle multiple z axis
        for v in ds.variables:
            if ds.variables[v].attrs.get('axis') == 'Z':
                ds.variables[v].attrs.pop('axis')
        ds.variables['elevation'].attrs['axis'] = 'Z'

        # set back attributes
        ds.attrs = attrs

        return ds

    @classmethod
    def postprocess(cls, ds, decode_times: bool = True):
        return ds


class S3SLRBTIR(S3SLRBT):

    @classmethod
    #@profile
    def open_dataset(cls,
                     filename_or_obj: T.Union[str, Path],
                     **kwargs):
        """open RBT files for IR thermal channels

        Parameters
        ----------
        path
            The path or url of the RBT dataset
        """
        return super(cls, S3SLRBTIR).open_dataset(
            filename_or_obj, channels='ir_thermal', **kwargs)


class S3SLRBTA(S3SLRBT):

    @classmethod
    #@profile
    def open_dataset(cls,
                     filename_or_obj: T.Union[str, Path],
                     **kwargs):
        """open RBT files for IR thermal channels

        Parameters
        ----------
        path
            The path or url of the RBT dataset
        """
        return super(cls, S3SLRBTA).open_dataset(
            filename_or_obj, channels='a_stripe', **kwargs)


class S3SLRBTB(S3SLRBT):

    @classmethod
    #@profile
    def open_dataset(cls,
                     filename_or_obj: T.Union[str, Path],
                     **kwargs):
        """open RBT files for B Stripe channels

        Parameters
        ----------
        path
            The path or url of the RBT dataset
        """
        return super(cls, S3SLRBTB).open_dataset(
            filename_or_obj, channels='b_stripe', **kwargs)