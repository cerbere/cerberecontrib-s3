from pathlib import Path
import re
import typing as T
import warnings

from cerberecontrib_ghrsst.reader.ghrsst import GHRSST
import cerberecontrib_s3.safes3.manifest as manifest


class S3SLIST(GHRSST):

    timestamp_re = r"[0-9]{8}T[0-9]{6}"
    pattern: str = re.compile(
       rf"S3A_SL_2_IST____{timestamp_re}_{timestamp_re}_{timestamp_re}.*SEN3")
    engine: str = "h5netcdf"
    description: str = ("Read Sentinel-3 SLSTR IST SAFE in Xarray as a single "
                        "Dataset")
    url: str = "https://link_to/your_backend/documentation"


    @classmethod
    #@profile
    def open_dataset(cls,
                     filename_or_obj: T.Union[str, Path],
                     **kwargs):
        """open ist files

        Parameters
        ----------
        path
            The path or url of the IST SAFE
        """
        kwargs['decode_timedelta'] = False
        kwargs['mask_and_scale'] = {'st_dtime': False}
        kwargs['engine'] = 'h5netcdf'

        ds = super().open_dataset(filename_or_obj, **kwargs)

        if (str(filename_or_obj).startswith('https') or
                str(filename_or_obj).startswith('s3')):
            fmanifest = filename_or_obj
        else:
            fmanifest = filename_or_obj.parent

        # add here some info from manifest if necessary
        try:
            manif, _ = manifest.parse(fmanifest)
            for att in ['processing_l1_facility_software_name',
                        'processing_l1_facility_software_version']:
                ds[att] = manif[att]['value']
        except Exception as e:
            warnings.warn("Missing manifest file. You won't get all global "
                          "attributes")

        ds.encoding['source'] = filename_or_obj

        ds = ds.rename_vars({'st_dtime': 'sst_dtime'})

        return ds

