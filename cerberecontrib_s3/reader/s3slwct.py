import re
import typing as T
from pathlib import Path

import numpy as np
import xarray as xr
from cerbere.reader.basereader import BaseReader

from cerberecontrib_s3.safes3.interpolation import (
    interpolate_meteorology_cerbere,
    interpolate_meteorology_scipy,
    interpolate_meteorology_xesmf)
from cerberecontrib_s3.safes3.slstr import wct


class S3SLWCT(BaseReader):

    timestamp_re = r"[0-9]{8}T[0-9]{6}"
    pattern: str = re.compile(
       rf"S3A_SL_2_WCT____{timestamp_re}_{timestamp_re}_{timestamp_re}.*SEN3")
    engine: str = "h5netcdf"
    description: str = ("Read Sentinel-3 SLSTR WCT SAFE container in Xarray "
                        "as a single Dataset")
    url: str = "https://link_to/your_backend/documentation"

    @classmethod
    #@profile
    def open_dataset(cls,
                     filename_or_obj: T.Union[str, Path],
                     interpolation_method='cerbere',
                     model_vars: bool = True,
                     **kwargs):
        """open wct files

        Parameters
        ----------
        path
            The path or url of the WCT dataset
        """
        tree = wct.open_wct(
            filename_or_obj,
            #chunks={'columns': 750, 'rows': 600},
            chunks={'columns': -1, 'rows': -1},
            drop_variables=['t_bound'], **kwargs)

        interpolation_methods = {
            "xesmf": interpolate_meteorology_xesmf,
            "scipy": interpolate_meteorology_scipy,
            "cerbere": interpolate_meteorology_cerbere,
        }
        interpolate_meteorology = interpolation_methods.get(
            interpolation_method)
        if interpolate_meteorology is None:
            raise ValueError(
                f"unknown interpolation method ({interpolation_method}),"
                f" choose one of {sorted(interpolation_methods)}"
            )

        # save global attributes
        attrs = tree['ir_thermal/nadir/n2'].ds.attrs

        if model_vars:
            ds = (
                tree
                .pipe(wct.combine_subgroups)
                .pipe(lambda dt: dt["/ir_thermal"])
                .pipe(lambda dt: interpolate_meteorology(dt["/ir_thermal"],
                                                         dt["/tie_point"]))
                .pipe(wct.pad_oblique)
                .pipe(wct.drop_common_variables)
                .pipe(lambda dt: xr.merge([node.ds for node in dt.subtree]))
            )
        else:
            ds = (
                tree
                .pipe(wct.combine_subgroups)
                .pipe(lambda dt: dt["/ir_thermal"])
                .pipe(wct.pad_oblique)
                .pipe(wct.drop_common_variables)
                .pipe(lambda dt: xr.merge([node.ds for node in dt.subtree]))
            )

        # convert time to datetime64
        ds = ds.assign_coords(
            time=(np.datetime64('2000-01-01T00:00:00') + ds.time).astype(
                'datetime64[ns]'))

        # fix some issues
        if 't_bound' in ds:
            # malformed time unit
            ds.t_bound.attrs.pop('units')
            ds.t_bound.encoding['units'] = ds.t_forecast.attrs['units']

        # can not handle multiple z axis
        for v in ds.variables:
            if ds.variables[v].attrs.get('axis') == 'Z':
                ds.variables[v].attrs.pop('axis')
        ds.variables['elevation'].attrs['axis'] = 'Z'

        # set back attributes
        ds.attrs = attrs

        return ds

    @classmethod
    def postprocess(cls, ds, decode_times: bool = True):
        return ds


class S3SLWCTCore(S3SLWCT):

    @classmethod
    #@profile
    def open_dataset(cls,
                     filename_or_obj: T.Union[str, Path],
                     **kwargs):
        kwargs.update(dict(model_vars=False))
        return super().open_dataset(filename_or_obj, **kwargs)
