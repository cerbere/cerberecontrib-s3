from pathlib import Path
import re
import typing as T
import warnings

from cerberecontrib_ghrsst.reader.ghrsst import GHRSST
import cerberecontrib_s3.safes3.manifest as manifest


class S3SLWST(GHRSST):

    timestamp_re = r"[0-9]{8}T[0-9]{6}"
    pattern: str = re.compile(
       rf"S3A_SL_2_WST____{timestamp_re}_{timestamp_re}_{timestamp_re}.*SEN3")
    engine: str = "h5netcdf"
    description: str = ("Read Sentinel-3 SLSTR WST SAFE in Xarray as a single "
                        "Dataset")
    url: str = "https://link_to/your_backend/documentation"

    @classmethod
    def open_as_safe(cls, filename_or_obj: T.Union[str, Path], **kwargs):
        """Open WST product from a SAFE URL.

        Only for old WST products, with combined nadir and dual view in one
        single netcdf file.
        """
        # read NetCDF inside SAFE
        ncfile = list(Path(filename_or_obj).glob('*.nc'))
        if len(ncfile) == 2:
            raise IOError(
                "This is a new convention WST product which containes nadir "
                "and dual view in separate netcdf files. You must provide the "
                "full path to the view file you want to open")
        if len(ncfile) != 1:
            raise IOError("Incorrect content in SAFE repository")

        return super().open_dataset(ncfile[0], **kwargs)


    @classmethod
    #@profile
    def open_dataset(cls,
                     filename_or_obj: T.Union[str, Path],
                     **kwargs):
        """open wst files

        Parameters
        ----------
        path
            The path or url of the WST SAFE
        """
        if not isinstance(filename_or_obj, Path):
            filename_or_obj = Path(filename_or_obj)

        if filename_or_obj.suffix == ".SEN3":
            ds = cls.open_as_safe(filename_or_obj, **kwargs)
            fmanifest = filename_or_obj
        else:
            # open a single view file
            ds = super().open_dataset(filename_or_obj, **kwargs)
            fmanifest = filename_or_obj.parent

        # add here some info from manifest if necessary
        try:
            manif, _ = manifest.parse(fmanifest)
            for att in ['processing_l1_facility_software_name',
                        'processing_l1_facility_software_version']:
                ds[att] = manif[att]['value']
        except:
            warnings.warn("Missing manifest file. You won't get all global "
                          "attributes")
            raise

        ds.encoding['source'] = filename_or_obj

        # fix lat/lon
        invalid = (ds['lat'].lat == -0.000999).values
        if invalid.sum() > 0:
            print(f'{invalid.sum()} lat/lon detected in WST dataset')

            ds['lat'] = ds['lat'].where(~invalid)
            ds['lon'] = ds['lon'].where(~invalid)

        return ds

