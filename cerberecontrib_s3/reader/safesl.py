import logging
import re
import typing as T
from collections import OrderedDict
from datetime import datetime
from enum import Enum
from pathlib import Path
from typing import Any, MutableMapping, Hashable

import dask
import numpy as np
import xarray as xr
from cerbere.dataset.dataset import OpenMode
from cerbere.dataset.field import Field
from cerbere.reader.basereader import BaseReader
from cerbere.reader.ghrsst import GHRSST

import cerberecontrib_s3.safes3.manifest as manifest


class ProductType(str, Enum):
    IR = "i"
    TDI = "c"
    A_STRIPE = "a"
    B_STRIPE = "b"


METEO = 'met_tx.nc'

PREFIX = {'n': 'Nadir', 'o': 'Oblique', 'x': ''}

PIXSYNC = {'in': 'PIXSYNC_i', 'io': 'PIXSYNC_i', 'an': 'PIXSYNC_a'}


class SAFESL(BaseReader):
    # S3A_SL_1_RBT____20161015T155616_20161015T155916_20161015T175945_0179_010_011_4139_MAR_O_NR_002.SEN3
    # S3A_SL_2_WCT____20210915T010108_20210915T010408_20210915T031331_0179_076_202_2700_MAR_O_NR_003.SEN3
    pattern: str = re.compile(r"^.*S3A_SL_[1,2]_((RB)|(WC))T____[0-9]{8}T[0-9]{6}_[0-9]{8}T[0-9]{6}_[0-9]{8}T[0-9]{6}_.*SEN3$")
    engine: str = "netcdf4"
    description: str = "Use SAFE RBT and WCT SLSTR product files in Xarray"
    url: str = "https://link_to/your_backend/documentation"
    # sub-product type (IR, A_STRIPE, B_STRIPE, TDI)
    _sl_type: str = None
    # for auxiliary field interpolation and slicing
    _aux_xy: Any = None
    _mode: OpenMode = OpenMode.READ_ONLY

    @classmethod
    def open_dataset(cls, filename_or_obj: T.Union[str, Path], **kwargs):
        if cls._mode != OpenMode.READ_ONLY:
            raise IOError("This dataset class can only be used in read_only "
                          "mode.")

        if cls._sl_type not in ProductType:
            raise ValueError(
                "Unknown SLSTR product type: {}".format(cls._sl_type))

        # infer list of files to open and update _url attribute
        # - all files from same SLType for nadir and oblique view
        nadir_files = list(
            Path(filename_or_obj).glob("*_{}n.nc".format(cls._sl_type))
        )
        oblique_files = list(
            Path(filename_or_obj).glob("*_{}o.nc".format(cls._sl_type))
        )
        met_files = list(Path(filename_or_obj).glob("*_tx.nc")) + \
                    list(Path(filename_or_obj).glob("*_tn.nc")) + \
                    list(Path(filename_or_obj).glob("*_to.nc"))

        logging.info('The following files will be opened:')
        for f in nadir_files + oblique_files:
            logging.info('  - {}'.format(f.name))

        safe_url = filename_or_obj

        ds = super(SAFESL, cls).open_mfdataset(nadir_files, preprocess=cls.preprocess, **kwargs)  # ???

        cls._std_dataset_o = super(SAFESL, cls).open_mfdataset(oblique_files, preprocess=cls.preprocess, **kwargs)  # ???

        cls._std_dataset_x = super(SAFESL, cls).open_mfdataset(met_files, preprocess=cls.preprocess, decode_times=False, **kwargs)  # ???

        logging.debug('Nadir dataset:\n ', ds)
        logging.debug('Oblique dataset:\n ', cls._std_dataset_o)
        logging.debug('Met dataset:\n ', cls._std_dataset_x)

        nadir_vars_to_rename = {
            old_name: new_name for old_name, new_name in
            {
                'rows': 'row',
                'columns': 'cell',
                f'elevation_{cls._sl_type}n': 'z',
                f'latitude_{cls._sl_type}n': 'lat',
                f'longitude_{cls._sl_type}n': 'lon',
                'orphan_pixels': 'orphan_pixels_n'
            }.items() if old_name in ds.variables
        }
        nadir_dims_to_rename = {
            old_name: new_name for old_name, new_name in
            {
                'rows': 'row',
                'columns': 'cell',
                f'elevation_{cls._sl_type}n': 'z',
                f'latitude_{cls._sl_type}n': 'lat',
                f'longitude_{cls._sl_type}n': 'lon',
                'orphan_pixels': 'orphan_pixels_n'
            }.items() if old_name in ds.dims
        }
        ds = ds.rename_vars(nadir_vars_to_rename)
        ds = ds.rename_dims(nadir_dims_to_rename)

        oblique_dims_to_rename = {
            old_name: new_name for old_name, new_name in
            {
                'rows': 'row',
                'columns': 'cell',
                'orphan_pixels': 'orphan_pixels_o'
            }.items() if old_name in cls._std_dataset_o.dims
        }
        cls._std_dataset_o = cls._std_dataset_o.rename_dims(oblique_dims_to_rename)

        cls._std_dataset_x = cls._std_dataset_x.rename_dims(
            {
                'rows': 'row',
                'columns': 'cell'
            }
        )

        # offset between nadir and oblique swath edge
        cls.oblique_offset = ds.track_offset - cls._std_dataset_o.track_offset

        for var in cls._std_dataset_o.variables:
            cls._load_oblique(ds=ds, field_name=var)

        # time coordinate - keep lazy operation with no masked array
        ds['time'] = xr.DataArray(
            data=cls._get_times(ds, as_masked_array=False),
            dims=ds['lat'].dims
        )

        # ... meteo
        cls._aux_xy = None
        for var in cls._std_dataset_x.variables:
            cls._load_met(ds, var)

        # save original encoding
        cls._save_encoding()  # ???

        # buffer for manifest content
        cls.manifest = None

        safe_name = Path(safe_url).parts[-1]
        ds.attrs['time_coverage_start'] = datetime.strptime(safe_name.split('_')[-11], '%Y%m%dT%H%M%S')
        ds.attrs['time_coverage_end'] = datetime.strptime(safe_name.split('_')[-10], '%Y%m%dT%H%M%S')

        return ds

    @classmethod
    def preprocess(cls, ds: xr.Dataset, **kwargs):
        prefix = Path(ds.encoding['source']).name.split('_')[0]
        new_dims = {}
        for d in ['uncertainties']:
            if d in ds.dims:
                new_dims[d] = '{}_{}'.format(prefix, d)
        if len(new_dims) > 0:
            ds = ds.rename_dims(new_dims)

        return ds

    @classmethod
    def get_values(cls, ds: xr.Dataset, field_name, **kwargs):
        if field_name in cls._std_dataset_x.variables:
            return cls._read_aux_values(ds, cls._std_dataset_x, field_name, **kwargs)
        else:
            return super(SAFESL, cls).get_values(field_name, **kwargs)  # ???
        raise ValueError(f'variable {field_name} not existing')

    @classmethod
    def _load_oblique(cls, ds: xr.Dataset, field_name):
        """load lazily an oblique field name into the dataset

        The loading includes left and right padding wrt track_offset
        """
        if 'cell' not in cls._std_dataset_o.variables[field_name].dims:
            # no padding required
            ds[field_name] = cls._std_dataset_o.variables[field_name]
            return

        # padding on left and right side
        def pad(arr, shape):
            return np.ones(shape) * arr.encoding.get(
                '_FillValue', np.ma.default_fill_value(arr))

        sizes = OrderedDict(cls._std_dataset_o[field_name].sizes)
        arr = cls._std_dataset_o[field_name]

        # left side padding
        shape = list(sizes.values())
        shape[list(sizes.keys()).index('cell')] = cls.oblique_offset
        left_padding = dask.delayed(pad)(arr, tuple(shape))
        left_arr = dask.array.from_delayed(
            left_padding, shape=tuple(shape), dtype=arr.dtype)

        # right side padding
        shape[list(sizes.keys()).index('cell')] = (
            ds.dims['cell'] - (cls._std_dataset_o.dims['cell'] + cls.oblique_offset)
        )
        right_padding = dask.delayed(pad)(arr, tuple(shape))
        right_arr = dask.array.from_delayed(
            right_padding, shape=tuple(shape), dtype=arr.dtype)

        # concatenate
        padded = dask.array.concatenate(
            [right_arr, arr, left_arr], axis=list(sizes.keys()).index('cell'))

        # as dataarray
        field = xr.DataArray(
            data=padded,
            name=arr.name,
            dims=arr.dims,
            attrs=arr.attrs)
        field.encoding = arr.encoding

        ds[field.name] = field

    @classmethod
    def _get_times(cls, ds: xr.Dataset, **kwargs):
        """build (lazily) a time coordinate variable"""
        as_masked_array = kwargs.get('as_masked_array', True)
        # to keep lazy operations
        kwargs['as_masked_array'] = False

        suffix = cls._sl_type
        prefix = PREFIX['n']

        SCANSYNC = cls.get_values(ds, 'SCANSYNC', **kwargs)[0].astype('timedelta64')
        PIXSYNC_i = cls.get_values(ds, 'PIXSYNC_{}'.format(suffix), **kwargs)[0].astype('timedelta64')
        first_scan_i = cls.get_values(ds, '{}_First_scan_{}'.format(prefix, suffix), **kwargs)
        first_min_ts = cls.get_values(ds, '{}_Minimal_ts_{}'.format(prefix, suffix), **kwargs)

        scan = cls.get_values(ds, 'scan_{}n'.format(suffix), **kwargs)
        pixel = cls.get_values(ds, 'pixel_{}n'.format(suffix), **kwargs)

        time = first_min_ts + (scan - first_scan_i) * SCANSYNC + pixel * PIXSYNC_i

        if not as_masked_array:
            return time

        return time.to_masked_array()

    @classmethod
    def _load_met(cls, ds: xr.Dataset, field_name):
        """load a met field name into the dataset

        Because of the required interpolation on the product grid, it is not
        possible to lazily load the data like for oblique fields
        """
        # fix invalid units
        if field_name == 't_bound':
            cls._std_dataset_x['t_bound'].attrs.pop('units')

        # add aux file coordinates to dataset
        coords = ['n_bound', 't_series', 'z_wind', 'z_soil']
        for coord in coords:
            if coord not in ds.coords:
                ds[coord] = cls._std_dataset_x[coord]

        # add aux file data variables
        if field_name not in coords:
            # self.add_field(self.get_field(fieldname))
            dims = dict(cls._std_dataset_x[field_name].sizes)
            fill_value = cls._std_dataset_x[field_name].encoding.get(
                '_FillValue',
                np.ma.default_fill_value(cls._std_dataset_x[field_name]))
            if 'cell' in dims:
                dims['cell'] = ds.dims['cell']
                dims['row'] = ds.dims['row']

            ds[field_name] = xr.DataArray(dask.array.from_delayed(
                    dask.delayed(np.ones(tuple(dims.values())) * fill_value),
                    shape=tuple(dims.values()),
                    dtype=cls._std_dataset_x[field_name].dtype),
                    dims=dims,
                    attrs=cls._std_dataset_x[field_name].attrs.copy()
            )
            ds[field_name].encoding = cls._std_dataset_x[field_name].encoding.copy()

    @classmethod
    def _product_for_dim(cls, ds: xr.Dataset, dim_name: str) -> xr.Dataset:
        """return which xarray Dataset a dimension belongs to"""
        if dim_name in ds.dims:
            return ds

        o_dims = cls._std_dataset_o.dims
        x_dims = cls._std_dataset_x.dims
        if all(dim_name in list for list in [o_dims, x_dims]) and o_dims[dim_name] != x_dims[dim_name]:
            raise ValueError('{0} belongs to two different subproducts with a different value'/format(dim_name))

        if dim_name in o_dims:
            return cls._std_dataset_o

        if dim_name in x_dims:
            return cls._std_dataset_x

    @classmethod
    def get_dimsize(cls, ds: xr.Dataset, dim_name: str) -> int:
        return cls._product_for_dim(ds=ds, dim_name=dim_name).dims[dim_name]

    @classmethod
    def _read_aux_values(cls, ds: xr.Dataset, dataset, field_name: str, index=None, **kwargs):
        """
        Args:
            dataset: handler containing the requested aux field
        """
        def _get_empty_values(handler):
            """build an empty result array in case of interpolation
            failure"""
            valdims = handler[field_name].sizes
            valshape = list(valdims.values())
            valshape[list(valdims.keys()).index('row')] = (
                    index['row'].stop - index['row'].start
            )
            valshape[list(valdims.keys()).index('cell')] = (
                    index['cell'].stop - index['cell'].start
            )
            values = np.ma.masked_all(
                shape=valshape,
                dtype=handler[field_name].dtype
            )
            return values

        # Dimensions and slices
        # Aux dimensions are left untouched in this demo so
        # slices argument refers to the original aux dimensions
        ####################################################################
        # TODOMAPPER : use mapper methods to get dimsizes, in this manner
        # slices = Slices(slices, dimsizes) allows to check the user slices
        # according to the dimensions exhibited by the mapper (dimensions
        # which may be different from original ones if for instance
        # you don't want to exhibit a z_wind dimension equal to 1)
        ####################################################################
        auxdims = dataset[field_name].sizes
        auxdimsizes = list(auxdims.values())

        if 'row' in auxdims:
            dimsizes = auxdimsizes[0:-2]
            dimsizes.append(cls.get_dimsize(ds, 'row'))
            dimsizes.append(cls.get_dimsize(ds, 'cell'))
        else:
            index = {k: v for k, v in index.items()
                     if k in dataset[field_name].dims}

            return dataset[field_name].isel(index)
            # return handler.get_values(fieldname, slices=slices)

        #slices = Slices(slices, dimsizes)
        _slices = {}
        for d in dataset[field_name].dims:
            if index is None or d not in index:
                _slices[d] = slice(0, cls.get_dimsize(ds, d))
            else:
                _slices[d] = slice(max(index[d].start, 0),
                                   min(index[d].stop, cls.get_dimsize(ds, d)))
        index = _slices
        or_index = _slices.copy()

        # Get x/y
        prod_type = cls._sltype.value
        y = cls.get_values(f'y_{prod_type}n', index=index)
        x = cls.get_values(f'x_{prod_type}n', index=index)

        if y.mask.all() or x.mask.all():
            # Useless to interpolate, return masked array
            return _get_empty_values(dataset)

        # Get aux x/y
        try:
            yaux, yrev, xaux, xrev = cls._get_aux_xy()
        except:
            logging.error("Bad coordinate values. Can't interpolate auxiliary "
                          "fields")
            return _get_empty_values(dataset)

        # Find aux bounding box according to x/y
        ymin = y.min()
        indymin = np.maximum(np.searchsorted(yaux, ymin) - 1, 0)
        ymax = y.max()
        indymax = np.minimum(np.searchsorted(yaux, ymax), yaux.size - 1)
        xmin = x.min()
        indxmin = np.maximum(np.searchsorted(xaux, xmin) - 1, 0)
        xmax = x.max()
        indxmax = np.minimum(np.searchsorted(xaux, xmax), xaux.size - 1)

        # Make sure aux bounding box is at least of size 2x2 points.
        # Bounding box of size 1 in xaux/yaux may happen when all y (resp. x)
        # are outside yaux (resp. xaux) range.
        # (e.g. indymin=indymax=0 or indymin=indymax=yaux.size-1)
        # In this case, an extra masked line or column will be added later in
        # vaux but 2 points will be needed in order to extrapolate xaux or
        # yaux.
        if indymin == indymax:
            if indymin == 0:
                indymax = indymax + 1
            else:
                indymin = indymin - 1
        if indxmin == indxmax:
            if indxmin == 0:
                indxmax = indxmax + 1
            else:
                indxmin = indxmin - 1

        # Get aux variable according to aux bounding box
        # And redefine aux x/y
        ####################################################################
        # TODOMAPPER : auxslices are the slices for reading into aux file.
        # For non geolocation dims (neither colums nor rows) it is built here
        # from slices variable (next line). You have to change this if the mapper
        # has removed some dimensions equal to 1 (dimensions which will not
        # appear in slices variable)
        ####################################################################
        if index is not None:
            auxslices = index
            if not yrev:
                auxyslice = slice(indymin, indymax + 1, 1)
            else:
                start, stop = yaux.size - 1 - indymin, yaux.size - 1 - indymax - 1
                if stop == -1:
                    stop = None
                auxyslice = slice(start, stop, -1)
            if not xrev:
                auxxslice = slice(indxmin, indxmax + 1, 1)
            else:
                start, stop = xaux.size - 1 - indxmin, xaux.size - 1 - indxmax - 1
                if stop == -1:
                    stop = None
                auxxslice = slice(start, stop, -1)
            auxslices['row'] = auxyslice
            auxslices['cell'] = auxxslice
            #auxslices.extend([auxyslice, auxxslice])
        else:
            auxslices = {}

        vaux = dataset[field_name].isel(**auxslices).to_masked_array()
        vaux = vaux.reshape((-1,) + vaux.shape[-2:])
        yaux = yaux[indymin: indymax + 1]
        xaux = xaux[indxmin: indxmax + 1]

        # Add masked lines/columns if x/y points are outside aux meteo bbox
        if ymin < yaux[0]:
            yaux = np.concatenate(([2 * yaux[0] - yaux[1]], yaux))
            shp = (vaux.shape[0], 1, vaux.shape[2])
            vaux = np.ma.hstack((
                np.ma.MaskedArray(np.zeros(shp), mask=True, dtype=vaux.dtype),
                vaux))
        if ymax > yaux[-1]:
            yaux = np.concatenate((yaux, [2 * yaux[-1] - yaux[-2]]))
            shp = (vaux.shape[0], 1, vaux.shape[2])
            vaux = np.ma.hstack((
                vaux,
                np.ma.MaskedArray(np.zeros(shp), mask=True, dtype=vaux.dtype)))
        if xmin < xaux[0]:
            xaux = np.concatenate(([2 * xaux[0] - xaux[1]], xaux))
            shp = (vaux.shape[0], vaux.shape[1], 1)
            vaux = np.ma.dstack((
                np.ma.MaskedArray(np.zeros(shp), mask=True, dtype=vaux.dtype),
                vaux))
        if xmax > xaux[-1]:
            xaux = np.concatenate((xaux, [2 * xaux[-1] - xaux[-2]]))
            shp = (vaux.shape[0], vaux.shape[1], 1)
            vaux = np.ma.dstack((
                np.ma.MaskedArray(np.zeros(shp), mask=True, dtype=vaux.dtype),
                vaux))

        # Get indices of surrounding aux meteo points for each x/y
        indy1 = np.searchsorted(yaux, y) - 1
        np.clip(indy1, 0, yaux.size - 2, out=indy1)
        indy2 = indy1 + 1
        indx1 = np.searchsorted(xaux, x) - 1
        np.clip(indx1, 0, xaux.size - 2, out=indx1)
        indx2 = indx1 + 1

        # Interpolate over x
        wx1 = (xaux[indx2] - x) / (xaux[indx2] - xaux[indx1])
        wx2 = (x - xaux[indx1]) / (xaux[indx2] - xaux[indx1])
        vy1 = (wx1[np.newaxis, :, :] * vaux[:, indy1, indx1] +
               wx2[np.newaxis, :, :] * vaux[:, indy1, indx2])
        vy2 = (wx1[np.newaxis, :, :] * vaux[:, indy2, indx1] +
               wx2[np.newaxis, :, :] * vaux[:, indy2, indx2])

        # When only one of the x surrounding points in aux is valid, mimic
        # nearest
        ind = np.where(
            ~np.ma.getmaskarray(x[np.newaxis, :, :]) & np.ma.getmaskarray(vy1))

        if ind[0].size != 0:
            near1 = np.abs(wx1[ind[1], ind[2]]) >= np.abs(wx2[ind[1], ind[2]])
            indn1 = [_ind[near1] for _ind in ind]
            vy1[tuple(indn1)] = vaux[:, indy1, indx1][tuple(indn1)]
            indn2 = [_ind[~near1] for _ind in ind]
            vy1[tuple(indn2)] = vaux[:, indy1, indx2][tuple(indn2)]

        ind = np.where(
            ~np.ma.getmaskarray(x[np.newaxis, :, :]) & np.ma.getmaskarray(vy2))

        if ind[0].size != 0:
            near1 = np.abs(wx1[ind[1], ind[2]]) >= np.abs(wx2[ind[1], ind[2]])
            indn1 = [_ind[near1] for _ind in ind]
            vy2[tuple(indn1)] = vaux[:, indy2, indx1][tuple(indn1)]
            indn2 = [_ind[~near1] for _ind in ind]
            vy2[tuple(indn2)] = vaux[:, indy2, indx2][tuple(indn2)]

        # Interpolate over y
        wy1 = (yaux[indy2] - y) / (yaux[indy2] - yaux[indy1])
        wy2 = (y - yaux[indy1]) / (yaux[indy2] - yaux[indy1])
        values = wy1[np.newaxis, :, :] * vy1 + wy2[np.newaxis, :, :] * vy2

        # When only one of the y surrounding points in aux is valid, mimic
        # nearest
        ind = np.where(
            ~np.ma.getmaskarray(y[np.newaxis, :, :]) &
            np.ma.getmaskarray(values))
        if ind[0].size != 0:
            near1 = np.abs(wy1[ind[1], ind[2]]) >= np.abs(wy2[ind[1], ind[2]])
            indn1 = tuple([_ind[near1] for _ind in ind])
            values[indn1] = vy1[indn1]
            indn2 = tuple([_ind[~near1] for _ind in ind])
            values[indn2] = vy2[indn2]

        # Reshape
        values = values.reshape(tuple(
            [_.stop - _.start for _ in or_index.values()]))

        return values


class SAFESLIR(SAFESL):
    _sl_type: ProductType = ProductType.IR


class SAFESL500A(SAFESL):
    _sl_type: ProductType = ProductType.A_STRIPE


class SAFESL500TDI(SAFESL):
    _sl_type: ProductType = ProductType.TDI


class SAFESLWCT(SAFESL):
    _sl_type: ProductType = ProductType.IR


class SAFESLWST(GHRSST):
    pass  # TODO
