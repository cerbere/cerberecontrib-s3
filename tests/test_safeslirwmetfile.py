"""

Test class for cerbere Sentinel-3 L1 mapper

:copyright: Copyright 2016 Ifremer / Cersat.
:license: Released under GPL v3 license, see :ref:`license`.

.. sectionauthor:: Jeff Piolle <jfpiolle@ifremer.fr>
.. codeauthor:: Jeff Piolle <jfpiolle@ifremer.fr>
"""
from pathlib import Path
import unittest

import numpy as np
import xarray as xr

from tests.checker import Checker, TEST_SUBSET


class S3L1RBTChecker(Checker, unittest.TestCase):
    """Test class for Sentinel-3 L1 RBT files"""
    def __init__(self, methodName="runTest"):
        super(S3L1RBTChecker, self).__init__(methodName)

    @classmethod
    def dataset(cls):
        """Return the dataset class name"""
        return 'SAFESLIRDataset'

    @classmethod
    def feature(cls):
        """Return the related feature class name"""
        return 'Swath'

    @classmethod
    def test_file(cls):
        """Return the name of the test file for this test"""
        return \
            "S3A_SL_1_RBT____20161015T155616_20161015T155916_20161015T175945_0179_010_011_4139_MAR_O_NR_002.SEN3"
        #return \
        #
        #    "S3A_SL_1_RBT____20210817T061128_20210817T061428_20210817T072141_0180_075_177_0000_MAR_O_NR_004.SEN3"

    @classmethod
    def download_url(cls):
        """Return the URL of the data test repository where to get the test
        files
        """
        return "ftp://ftp.ifremer.fr/ifremer/cersat/projects/cerbere/test_data/"

    def test_extract_and_save_a_subset(self):
        super(S3L1RBTChecker, self).test_extract_and_save_a_subset()

        # open created subset
        dst = xr.open_dataset(TEST_SUBSET)
        slices = eval(dst.attrs['slices'])

        # open reference nadir file
        ref = xr.open_mfdataset(
            Path(self.testfile).glob('*in.nc'),
            preprocess=self.load_dataset(self.dataset()).preprocess,)

        refslices = {'columns': slices['cell'], 'rows': slices['row']}
        refsubset = ref.loc[refslices]

        # compare subsets
        ref_variables = list(dst.variables.keys())
        renamed = {'elevation_in': 'z', 'latitude_in': 'lat',
                   'longitude_in': 'lon'}
        for v in refsubset.variables.keys():
            print("checking ", v)

            # check all variables there
            self.assertIn(renamed.get(v, v), ref_variables)

            sub_arr = dst.variables[renamed.get(v, v)]
            ref_arr = refsubset.variables[v]

            # check shape
            self.assertEqual(sub_arr.shape, ref_arr.shape, v)

            # check dtype
            self.assertEqual(sub_arr.dtype, ref_arr.dtype, v)

            def fill_value(arr):
                return arr.encoding.get(
                    '_FillValue',
                    arr.attrs.get('_FillValue', None)
                )
            if v == 'Nadir_Maximal_ts_i':
                print(v)

            # check _FillValue
            if ref_arr.dtype in [np.dtype(np.float32), np.dtype(np.float64)]:
                # xarray enforces _FillValue for floats
                self.assertTrue(
                    fill_value(sub_arr) is not None
                )
            elif not np.issubdtype(sub_arr.dtype, np.datetime64):
                self.assertEqual(fill_value(sub_arr), fill_value(ref_arr), v)

            # check data
            if '_FillValue' in ref_arr.encoding:
                # some nan instead of _FillValue in reference files...

                print(np.ma.fix_invalid(ref_arr))
                np.testing.assert_array_equal(
                    ref_arr, sub_arr, v)
            else:
                np.testing.assert_array_equal(ref_arr, sub_arr, v)

    def test_aux_field(self):
        datasetobj = self.datasetclass(self.testfile)
        feature = self._create_feature(datasetobj)

        field = feature.get_field('east_west_stress_tx')
        values = feature.get_values('east_west_stress_tx')

        np.testing.assert_array_equal(field.get_values(), values)
        print(values.max(), values.min())

        # check subset
        slices = {'row': slice(10, 20, 1), 'cell': slice(10, 20, 1)}
        print("Subset: ", slices)
        subset = feature.extract(index=slices)

        np.testing.assert_array_equal(
            subset.get_values('east_west_stress_tx'),
            values[:, slice(10, 20, 1), slice(10, 20, 1)])
