import cerbere
import matplotlib.pyplot as plt
import datetime

start = datetime.datetime.now()

print('Read')
dst = cerbere.open_dataset(
    '/home/jfpiolle/git/cerbere/tests/data/SAFESLIRDataset'
    '/S3A_SL_1_RBT____20161015T155616_20161015T155916_20161015T175945_0179_010_011_4139_MAR_O_NR_002.SEN3/',
    'SAFESLIRDataset'
)
print("Done", (datetime.datetime.now() - start).total_seconds())
start = datetime.datetime.now()

wind1 = dst.get_values(
    'u_wind_tx',
    index={'row': slice(10,20), 'cell': slice(10, 20)})

print('EXTRACT')
subset = dst.extract(index={'row': slice(10,20), 'cell': slice(10, 20)})
#subset.save("toto.nc")

print("Compare subset vs extract")
diff = wind1 - subset.get_values('u_wind_tx')
print(diff.min(), diff.max())

print('Test padding 3')
subset = dst.extract(index={'row': slice(-2,12), 'cell': slice(1490, 1510)},
                     padding=True)

print('Test padding 1')
subset = dst.extract(index={'row': slice(10,20), 'cell': slice(10, 20)},
                     padding=True)

print('Test padding 2')
subset = dst.extract(index={'row': slice(10,20), 'cell': slice(1490, 1510)},
                     padding=True)



print("Done", (datetime.datetime.now() - start).total_seconds())


start = datetime.datetime.now()

print('SUBSET 1')
values_all = dst.get_values(
    'solar_radiation_tx')
print("Done", (datetime.datetime.now() - start).total_seconds())
start = datetime.datetime.now()

plt.imshow(values_all[0,:,:])
plt.show()


values = dst.get_values(
    'solar_radiation_tx',
    index={'row': slice(500, 1000, None),
           'cell': slice(1354, 1500, None)})
plt.imshow(values[0, :, :])
plt.show()


diff = values_all[2, 500:1000, 1354:1500] - values[2, :, :]
print("DIFF 1 ", diff.min(), diff.max())
plt.imshow(diff[:, :], vmin=-1, vmax=1, cmap='jet')
plt.colorbar()
plt.show()
print(diff)

values_all = dst.get_values(
    'total_column_water_vapour_tx')
plt.imshow(values_all[0,:,:])
plt.show()

values = dst.get_values(
    'total_column_water_vapour_tx',
    index={'cell': slice(1354, 1500, None)})
plt.imshow(values[0,:,:])
plt.show()

diff = values_all[0, :, slice(1354, 1500, None)] - values[0,:,:]
plt.imshow(diff)
plt.show()
print(diff.min(), diff.max())

print(values)




values = dst.get_values(
    'total_column_water_vapour_tx',
    index={'cell': slice(1354, 1500, None)}, padding=True)

