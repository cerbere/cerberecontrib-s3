from cerbere.feature.cswath import Swath
from cerbere.reader import pytest_reader as pr

TEST_FILE = 'S3A_SL_2_IST____20250120T104435_20250120T104735_20250120T134410_0179_121_322______EUM_P_NR_004.SEN3/20250120104435-EUM-L2P_GHRSST-STskin-SLSTRA_NRT_DUAL_NH_SST_IST_20250120134410-v02.1-fv01.0.nc'


@pr.pytest.fixture(scope='module')
def test_file():
    return TEST_FILE


@pr.pytest.fixture(scope='module')
def reader():
    return 'S3SLIST'


@pr.pytest.fixture(scope='module')
def feature_class():
    return Swath
