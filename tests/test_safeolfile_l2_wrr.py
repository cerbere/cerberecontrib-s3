"""

Test class for cerbere Sentinel-3 OLCI L2 WRR

:copyright: Copyright 2016 Ifremer / Cersat.
:license: Released under GPL v3 license, see :ref:`license`.

.. sectionauthor:: Jeff Piolle <jfpiolle@ifremer.fr>
.. codeauthor:: Jeff Piolle <jfpiolle@ifremer.fr>
"""
import unittest

from cerberecontrib_s3.mapper.checker import Checker


class S3OLWRRChecker(Checker, unittest.TestCase):
    """Test class for Sentinel-3 OLCI L2 WRR files"""

    def __init__(self, methodName="runTest"):
        super(S3OLWRRChecker, self).__init__(methodName)

    @classmethod
    def mapper(cls):
        """Return the mapper class name"""
        return 'SAFEOLFile'

    @classmethod
    def datamodel(cls):
        """Return the related datamodel class name"""
        return 'Swath'

    @classmethod
    def test_file(cls):
        """Return the name of the test file for this test"""
        return "S3A_OL_2_WFR____20161105T000231_20161105T000531_20161105T014220_0179_010_301_4139_MAR_O_NR_002.SEN3"

    @classmethod
    def download_url(cls):
        """Return the URL of the data test repository where to get the test
        files
        """
        return "ftp://ftp.ifremer.fr/ifremer/cersat/projects/cerbere/test_data/"
