from cerbere.feature.cswath import Swath
from cerbere.reader import pytest_reader as pr

TEST_FILE = 'S3A_SL_2_WST____20250120T091236_20250120T091536_20250120T114407_0180_121_321______EUM_P_NR_004.SEN3/20250120091236-EUM-L2P_GHRSST-SSTskin-SLSTRA_NRT_DUAL_20250120114407-v02.1-fv01.0.nc'


@pr.pytest.fixture(scope='module')
def test_file():
    return TEST_FILE


@pr.pytest.fixture(scope='module')
def reader():
    return 'S3SLWST'


@pr.pytest.fixture(scope='module')
def feature_class():
    return Swath
