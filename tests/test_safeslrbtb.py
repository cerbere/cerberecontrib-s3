"""
Test class for cerbere Sentinel-3 SLSTR L1 RBT dataset

:copyright: Copyright 2023 Ifremer / Cersat.
:license: Released under GPL v3 license, see :ref:`license`.

.. sectionauthor:: Jeff Piolle <jfpiolle@ifremer.fr>
.. codeauthor:: Jeff Piolle <jfpiolle@ifremer.fr>
"""
from pathlib import Path

from cerbere.feature.cswath import Swath
from cerbere.reader import pytest_reader as pr


@pr.pytest.fixture(scope='session')
def test_data_dir():
    return Path(__file__).parent / 'data'


@pr.pytest.fixture(scope='module')
def input_file(test_data_dir):
    return Path(
    test_data_dir,
        'S3SLRBT',
        'S3A_SL_1_RBT____20250120T091236_20250120T091536_20250120T111118_0179_121_321_2340_MAR_O_NR_004.SEN3',
    )


@pr.pytest.fixture(scope='module')
def reader():
    return 'S3SLRBTB'

@pr.pytest.fixture(scope='module')
def feature_class():
    return Swath
