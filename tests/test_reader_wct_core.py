from cerbere.reader.pytest_reader import *

from cerbere.feature.cswath import Swath

TEST_FILE = 'S3A_SL_2_WCT____20210915T010108_20210915T010408_20210915T031331_0179_076_202_2700_MAR_O_NR_003.SEN3'


@pytest.fixture(scope='module')
def test_file():
    return TEST_FILE


@pytest.fixture(scope='module')
def reader():
    return 'S3SLWCTCore'


@pytest.fixture(scope='module')
def feature_class():
    return Swath
