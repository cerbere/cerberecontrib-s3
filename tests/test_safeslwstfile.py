"""

Test class for cerbere Sentinel-3 SLSTR L2 WST dataset

:copyright: Copyright 2023 Ifremer / Cersat.
:license: Released under GPL v3 license, see :ref:`license`.

.. sectionauthor:: Jeff Piolle <jfpiolle@ifremer.fr>
.. codeauthor:: Jeff Piolle <jfpiolle@ifremer.fr>
"""
from cerbere.feature.cswath import Swath
from cerbere.reader.pytest_reader import *


@pytest.fixture(scope='session')
def test_data_dir():
    return Path(__file__).parent / 'data'


@pytest.fixture(scope='module')
def input_file(test_data_dir):
    return Path(
    test_data_dir,
    'AVISOSARAL',
    'SRL_GPS_2PfP019_0523_20141222_111417_20141222_120436.CNES.nc',
)


@pytest.fixture(scope='module')
def reader():
    return 'AVISOSARAL1Hz'


@pytest.fixture(scope='module')
def feature_class():
    return Swath

from pathlib import Path
import unittest

import numpy as np
import xarray as xr

from cerbere.dataset.checker import Checker, TEST_SUBSET


class S3L2WSTChecker(Checker, unittest.TestCase):
    """Test class for Sentinel-3 WST files"""
    def __init__(self, methodName="runTest"):
        super(S3L2WSTChecker, self).__init__(methodName)

    @classmethod
    def dataset(cls):
        """Return the dataset class name"""
        return 'SAFESLWSTDataset'

    @classmethod
    def feature(cls):
        """Return the related feature class name"""
        return 'Swath'

    @classmethod
    def test_file(cls):
        """Return the name of the test file for this test"""
        return \
            "S3A_SL_2_WST____20210915T010108_20210915T010408_20210915T031329_0179_076_202_2700_MAR_O_NR_003.SEN3"

    @classmethod
    def download_url(cls):
        """Return the URL of the data test repository where to get the test
        files
        """
        return "ftp://ftp.ifremer.fr/ifremer/cersat/projects/cerbere/test_data/"

