"""
Test class for cerbere Sentinel-3 SLSTR L2 WCT dataset

:copyright: Copyright 2023 Ifremer / Cersat.
:license: Released under GPL v3 license, see :ref:`license`.

.. sectionauthor:: Jeff Piolle <jfpiolle@ifremer.fr>
.. codeauthor:: Jeff Piolle <jfpiolle@ifremer.fr>
"""
from cerbere.feature.cswath import Swath
from cerbere.reader.pytest_reader import *


@pytest.fixture(scope='session')
def test_data_dir():
    return Path(__file__).parent / 'data'


@pytest.fixture(scope='module')
def input_file(test_data_dir):
    return Path(
    test_data_dir,
        'S3SLWCT',
        'S3A_SL_2_WCT____20210915T010108_20210915T010408_20210915T031331_0179_076_202_2700_MAR_O_NR_003.SEN3',
    )


@pytest.fixture(scope='module')
def reader():
    return 'S3SLWCT'

@pytest.fixture(scope='module')
def feature_class():
    return Swath

