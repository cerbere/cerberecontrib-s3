from cerbere.feature.cswath import Swath
from cerbere.reader.pytest_reader import *


@pytest.fixture(scope='session')
def test_data_dir():
    # return Path(__file__).parent / 'data'
    return Path('/home/csouvy/Documents/CERSAT/cerbere/test/data/s3')
    # return Path('/data/cerbere/test/s3')


@pytest.fixture(scope='module')
def input_file(test_data_dir):
    return Path(
        test_data_dir,
        'RBT',
        # /home/datawork-cersat-public/cache/users/jfpiolle/felyx/data/s3a/level1/sl_1_rbt____o_nr/data/s3a.full/ => /data/cerbere/test/s3/RBT
        # 'S3A_SL_1_RBT____20161015T155616_20161015T155916_20161015T175945_0179_010_011_4139_MAR_O_NR_002.SEN3',
        'S3A_SL_1_RBT____20220401T002133_20220401T002433_20220401T023445_0179_083_330_2340_MAR_O_NR_004.SEN3'
    )


@pytest.fixture(scope='module')
def reader():
    return 'SAFESL500A'


@pytest.fixture(scope='module')
def feature_class():
    return Swath
