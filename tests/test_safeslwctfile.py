"""

Test class for cerbere Sentinel-3 SLSTR L2 WCT dataset

:copyright: Copyright 2016 Ifremer / Cersat.
:license: Released under GPL v3 license, see :ref:`license`.

.. sectionauthor:: Jeff Piolle <jfpiolle@ifremer.fr>
.. codeauthor:: Jeff Piolle <jfpiolle@ifremer.fr>
"""
from pathlib import Path
import unittest

import numpy as np
import xarray as xr

from cerbere.dataset.checker import Checker, TEST_SUBSET


class S3L2WSTChecker(Checker, unittest.TestCase):
    """Test class for Sentinel-3 WCT files"""
    def __init__(self, methodName="runTest"):
        super(S3L2WSTChecker, self).__init__(methodName)

    @classmethod
    def dataset(cls):
        """Return the dataset class name"""
        return 'SAFESLWCTDataset'

    @classmethod
    def feature(cls):
        """Return the related feature class name"""
        return 'Swath'

    @classmethod
    def test_file(cls):
        """Return the name of the test file for this test"""
        return \
            'S3A_SL_2_WCT____20210915T010108_20210915T010408_20210915T031331_0179_076_202_2700_MAR_O_NR_003.SEN3'

    @classmethod
    def download_url(cls):
        """Return the URL of the data test repository where to get the test
        files
        """
        return "ftp://ftp.ifremer.fr/ifremer/cersat/projects/cerbere/test_data/"

